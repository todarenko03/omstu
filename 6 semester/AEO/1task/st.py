import streamlit as st
from new_de import *
import time
import plotly.graph_objects as go
import networkx as nx

def main():
    st.title("DE for TCP")
    population_size = st.sidebar.slider("Population Size", min_value=10, max_value=100, value=50)
    generations = st.sidebar.slider("Generations", min_value=10, max_value=1000, value=100)
    cr = st.sidebar.slider("Crossover rate", min_value=0.1, max_value=1.0, step=0.1)
    f = st.sidebar.slider("Mutation function", min_value=0.1, max_value=1.0, value=0.8, step=0.1)
    indpb = st.sidebar.slider("Mutation probability", min_value=0.1, max_value=1.0, value=0.2, step=0.1)
    num_of_cities = st.sidebar.slider("Number of cities", min_value=1, max_value=100, value=5, step=1)
    distance_matrix = [[0 for i in range(num_of_cities)] for j in range(num_of_cities)]
    st.sidebar.subheader("Distance matrix")
    time_of_the_execute = 0
    for i in range(num_of_cities):
        distance_matrix[i][i] = int(1e+6)
        for j in range(i + 1, num_of_cities):
            value = st.sidebar.number_input(f"Distance from city {i} to city {j}", min_value=0, max_value=10000, value=(i + 1) * (j + 1))
            distance_matrix[i][j] = value
            distance_matrix[j][i] = value
    
    best_route, best_distance = [], 0
    if st.sidebar.button("Calculate"):  
        start = time.time()    
        best_route, best_distance = differential_evolution(distance_matrix, population_size, generations, cr, f, indpb)
        finish = time.time()
        time_of_the_execute = finish - start
    
    st.table(distance_matrix)
    st.subheader("Best Route:")
    st.write(best_route)
    st.subheader("Best Distance:")
    st.write(best_distance)
    st.write("Time of the execute")
    st.write(time_of_the_execute)
    st.set_option('deprecation.showPyplotGlobalUse', False)
    res_matr = [[0 for i in range(len(distance_matrix))] for j in range(len(distance_matrix))]
    for i in range(len(best_route) - 2):
        res_matr[i][i+1] = distance_matrix[i][i+1]
        res_matr[-1][0] = distance_matrix[-1][0] 
        # res_matr[i+1][i] = distance_matrix[i][i+1]  
    # res_matr[len(best_route) - 1][len(best_route) - 2] = distance_matrix[len(best_route) - 1][len(best_route) - 2]
    # res_matr[i+1][i] = distance_matrix[i][i+1]
    res_matrix = np.array(res_matr)
    G=nx.from_numpy_array(res_matrix, create_using=nx.DiGraph)
    edge_nodes = set(G)
    pos = nx.circular_layout(G.subgraph(edge_nodes))
    nx.draw(G, pos, with_labels=1, node_color='g', node_size=700)
    edge_labels = nx.get_edge_attributes(G, "weight")
    nx.draw_networkx_edge_labels(G, pos, edge_labels)
    st.pyplot()


if __name__ == '__main__':
      main()
