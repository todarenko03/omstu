public interface Printable {
    void print();
}

public abstract class Animal {
    protected String name;

    public Animal(String name) {
        this.name = name;
    }

    public abstract void sound();
}

public class Dog extends Animal implements Printable {
    public Dog(String name) {
        super(name);
    }

    @Override
    public void sound() {
        System.out.println("Woof!");
    }

    @Override
    public void print() {
        System.out.println("Printing dog: " + name);
    }
}

public class Cat extends Animal implements Printable {
    public Cat(String name) {
        super(name);
    }

    @Override
    public void sound() {
        System.out.println("Meow!");
    }

    @Override
    public void print() {
        System.out.println("Printing cat: " + name);
    }
}

public class Print implements Printable {
    @Override
        public void print() {
            System.out.println("Printable impl);
        }
}
public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog("Buddy");
        Cat cat = new Cat("Whiskers");

        dog.sound();
        dog.print();

        cat.sound();
        cat.print();
    }
}