from elevator import Elevator


class Building:
    def __init__(self, num_of_floors, elevator1_position, elevator2_position):
        self.elevator1 = Elevator(num_of_floors=num_of_floors, init_position=elevator1_position)
        self.elevator2 = Elevator(num_of_floors=num_of_floors, init_position=elevator2_position)

    def start(actions):
        pass

    