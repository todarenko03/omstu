class Graph:
    def __init__(self):
        self.graph = []
        self.orig_graph = []

    def add_nodes(self, i):
        self.graph.append([int(x) for x in i.split()])

    def floyd(self, start, finish):
        start -= 1
        finish -= 1
        next = [[i for i in range(len(self.graph))] for j in range(len(self.graph))]
        for z in range(len(self.graph)):
            for i in range(len(self.graph)):
                for j in range(len(self.graph)):
                    for k in range(len(self.graph)):
                        if self.graph[i][k] > 0 and self.graph[k][j] > 0:
                            if self.graph[i][j] == -1:
                                self.graph[i][j] = self.graph[i][k] + self.graph[k][j]
                            if self.graph[i][j] > self.graph[i][k] + self.graph[k][j]:
                                self.graph[i][j] = self.graph[i][k] + self.graph[k][j]
                                next[i][j] = k
        print(self.graph)
        c = start
        while c != finish:
            print(c+1, end=" ")
            c = next[c][finish]
        print(finish+1)


with open('tt.txt', 'r') as f:
    f1 = f.readlines()

g = Graph()
for i in f1:
    g.add_nodes(i)
g.floyd(4, 3)
