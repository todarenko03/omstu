#! pip install bigartm
import re
import pandas as pd
from nltk import word_tokenize
from nltk.corpus import stopwords
import nltk
import artm
from nltk.stem import WordNetLemmatizer
from artm import BatchVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import fetch_20newsgroups

dataset = fetch_20newsgroups(remove=('headers', 'footers', 'qutes'))

df = pd.DataFrame({'text': dataset.data, 'target': dataset.target})

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

def preprocess(text: str) -> str:
    pattern = re.compile(r'[^a-z]+')
    text = text.lower()
    text = pattern.sub(' ', text).strip()

    word_list = word_tokenize(text)
    stopwords_list = set(stopwords.words('english'))

    word_list = [word for word in word_list if word not in stopwords_list]
    word_list = [word for word in word_list if len(word) > 2]

    lemma = WordNetLemmatizer()
    word_list = [lemma.lemmatize(word) for word in word_list]
    text= ' '.join(word_list)

    return text

df['text'] = df['text'].apply(lambda x: preprocess(x))

vectorizer = CountVectorizer(ngram_range=(1,2), max_features=5200)
m = vectorizer.fit_transform(df['text'])

n_wd = m.todense().T

vocabulary = vectorizer.get_feature_names_out()

batch_vectorizer = BatchVectorizer(data_format='bow_n_wd', n_wd=n_wd, vocabulary=vocabulary, target_folder='batches')

dictionary = artm.Dictionary()
dictionary.gather(data_path='batches')

model = artm.ARTM(num_topics=15)
model.scores.add(artm.PerplexityScore(name='perplexity_score'))
model.scores.add(artm.TopTokensScore(name='top_tokens_score'))

model.num_document_passes = 3
model.initialize(dictionary=dictionary)
model.fit_offline(batch_vectorizer, num_collection_passes=5)

print(model.score_tracker['perplexity_score'].value)
