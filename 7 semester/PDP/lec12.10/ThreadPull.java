import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws InterruptedException{
        public static void longRunngingMethod3() throws InterruptedException{
            ExecutorService s = Executor.newFixedThreadPool(3);
            List<Future<Integer>> results = new ArrayList();
            for (int i = 0; i < 100; i++) {
                results.add(s.submit(() -> 1));
            }    
            s.shutdown();

            int sum = 0;
            for (Future<Integer> r: results) {
                try {
                    sum += r.get();
                } catch (InterruptedException | ExecutionException e) {}
            }

            System.out.println(sum);
        }
    }
}
