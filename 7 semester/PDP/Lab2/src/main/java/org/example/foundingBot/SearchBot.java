package org.example.foundingBot;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class SearchBot implements ISearchBot {

    private final Pattern patternClass = Pattern.compile("class\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternInterface = Pattern.compile("interface\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassExtends = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassImplements = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassImplementsAfterExtends = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassExtendsAfterImplements = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)");

    public void search(final File folder, final Map<String, List<String>> hierarchy)  {
        try {
            for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
                if (fileEntry.isDirectory()) {
                    search(fileEntry, hierarchy);
                } else {
                    final String text = String.join(" ", Files.readAllLines(Paths.get(fileEntry.getPath()), StandardCharsets.UTF_8));
                    patternClass.matcher(text)
                                    .results()
                            .forEach(
                                    res -> {
                                        hierarchy.put(res.group(1), hierarchy.getOrDefault(res.group(1), new ArrayList<>()));
                            });
                    patternInterface.matcher(text)
                            .results()
                            .forEach(
                                    res -> {
                                        hierarchy.put(res.group(1), hierarchy.getOrDefault(res.group(1), new ArrayList<>()));
                                    });
                    patternClassExtends.matcher(text)
                            .results()
                            .forEach(
                                    res -> {
                                        List<String> list = hierarchy.getOrDefault(res.group(2), new ArrayList<>());
                                        list.add(res.group(1));
                                        hierarchy.put(res.group(2), list);
                                    }
                            );
                    patternClassImplements.matcher(text)
                            .results()
                            .forEach(
                                    res -> {
                                        List<String> list = hierarchy.getOrDefault(res.group(2), new ArrayList<>());
                                        list.add(res.group(1));
                                        hierarchy.put(res.group(2), list);
                                    }
                            );
                    patternClassImplementsAfterExtends.matcher(text)
                            .results()
                            .forEach(
                                    res -> {
                                        List<String> list = hierarchy.getOrDefault(res.group(3), new ArrayList<>());
                                        list.add(res.group(1));
                                        hierarchy.put(res.group(3), list);
                                    }
                            );
                    patternClassExtendsAfterImplements.matcher(text)
                            .results()
                            .forEach(
                                    res -> {
                                        List<String> list = hierarchy.getOrDefault(res.group(3), new ArrayList<>());
                                        list.add(res.group(1));
                                        hierarchy.put(res.group(3), list);
                                    }
                            );
                }
            }
        } catch (IOException e) {
            System.out.println("Can't open" + e.getMessage());
        }
    };
}
