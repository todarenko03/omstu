package org.example.foundingBot;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public interface ISearchBot {
    void search(Map<String, List<String>> hierarchy) throws InterruptedException, ExecutionException;
}
