import time
import random
import matplotlib.pyplot as plt

def search_max(lines: list[tuple[int]]) -> int:
    lines.sort(key=lambda x: x[1])
    result = []
    right_border = 0
    for line in lines:
        if line[0] >= right_border:
            result.append(line)
            right_border = line[1]

    return len(result)


print(search_max([(1, 2), (2, 3), (2, 4), (3, 5)]))


def search_max2(schedule: list[tuple[tuple[int]]]) -> int:
    lines = []
    for el in schedule:
        line = [el[0][0] * 60 + el[0][1], el[1][0] * 60 + el[1][1]]
        lines.append(line)
    lines.sort(key=lambda x: x[1])
    result = []
    right_border = 0
    for line in lines:
        if line[0] >= right_border:
            result.append(line)
            right_border = line[1]

    return len(result)

print(search_max2([((1, 35), (2, 40)), ((2, 39), (4, 13)), ((2, 45), (3, 45))]))

def search_min(lines: list[tuple[int]], interval: tuple[int]) -> int:
    count = 0
    temp_right = interval[0]
    temp_lines = []
    while temp_right < interval[1]:
        if len(lines) == 0:
            return None
        for line in lines:
            if line[0] <= temp_right:
                temp_lines.append(lines.pop(lines.index(line)))
        temp_lines.sort(reverse=True)
        temp_right += temp_lines[0][1]
        count += 1 

    return count

print(search_min([(1, 2), (1, 4), (3, 5), (4, 5)], (1, 5)))


times1 = []
times2 = []
x = [i for i in range(100, 10000, 200)]
for i in x:
    start1 = time.time()
    search_max([(random.randint(0, (j + 5) // 3), random.randint((j + 5) // 3, j - 1)) for j in range(5, i)])
    finish1 = time.time()
    times1.append(finish1 - start1)
    start2 = time.time()
    search_min([(random.randint(0, (j + 5) // 3), random.randint((j + 5) // 3, j - 1)) for j in range(5, i)], (random.randint(0, i - i // 2), random.randint(0, i)))
    finish2 = time.time()
    times2.append(finish2 - start2)

plt.plot(x, times1)
plt.plot(x, times2)
plt.xlabel = "size"
plt.ylabel = "time"
plt.legend(["search_max", "search_min"])
plt.show()
