package org.example.transformer;

public interface ITransformer {
    String transform(String text, String template, String replacing);
}
