package org.example.wordCounter;

import java.util.Map;

public interface IWordCounter {
    Map<String, Integer> calculate(String text);
}
