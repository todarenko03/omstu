public class Main {
    public static void main(String[] args) {
        System.out.println("Hello, world!");

        MyList list = new MyList();
        list.putIfAbsent(10);
        list.add(20);
        list.add(30);

        List<Integer> items = new ArrayList<>();
        for (Integer x: items) {
            if (x < 10) {
                items.delete(x);
            }
        }

        List<Integer> filteredItems = items.stream()
            .filter(x -> x < 10)
            .collect(Collectors.toList());

        List<Integer> itemsToDelete = new ArrayList<>();
        for (Integer x: items) {
            if (x < 10) {
                itemsToDelete.add(x);
            }
        }
        itemsToDelete.forEach(x -> items.delete(x));
    }
}
