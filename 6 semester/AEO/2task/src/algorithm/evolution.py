import numpy as np
from task import Task
from statemachine import StateMachine
from cross import cross_machines
from mutation import random_mutation


class Evolution:
    def __init__(self, matrix, n_states=7, n_machines=10, n_turns=900):
        self.matrix = matrix
        self.task = Task(matrix)
        self.n_states = n_states
        self.n_machines = n_machines
        self.best_score = 0
        self.best_machine = None
        self.best_road = None
        self.n_turns = n_turns
        self.machines = []
        self.epoch_score = np.zeros(self.n_machines)
        self.epoch_roads = []
        for _ in range(n_machines):
            self.machines.append(StateMachine(self.task, self.generate_states()))

    def generate_states(self):
        states = np.zeros((self.n_states, 4))
        for i in range(self.n_states):
            move0, move1 = np.random.randint(low=0, high=3, size=2)
            available_states = np.delete(np.arange(0, self.n_states), i)
            state0, state1 = np.random.choice(available_states, size=2)
            states[i] = np.array([move0, state0, move1, state1])
        return states

    def solve_machines(self):
        for i in range(self.n_machines):
            road, score = self.machines[i].solve(self.n_turns)
            self.epoch_roads.append(road)
            self.epoch_score[i] = score
            self.task.restart_task(self.matrix)

    def epoch(self):
        self.epoch_score = np.zeros(self.n_machines)
        self.epoch_roads = []
        self.solve_machines()
        indexes_and_scores = np.vstack((self.epoch_score, np.arange(0, self.n_machines))).T
        indexes_and_scores = sorted(indexes_and_scores, key=lambda x: x[0], reverse=True)
        indexes_and_scores = np.array(indexes_and_scores)
        indexes_and_scores = indexes_and_scores.astype(int)
        if indexes_and_scores[0][0] >= self.best_score:
            self.best_machine = self.machines[indexes_and_scores[0][1]]
            self.best_road = self.epoch_roads[indexes_and_scores[0][1]]
            self.best_score = indexes_and_scores[0][0]
        new_population = []
        best_machine, worst_machine = self.machines[indexes_and_scores[0][1]], self.machines[indexes_and_scores[-1][1]]
        states_kid_1, states_kid_2 = cross_machines(best_machine.states, worst_machine.states)
        new_population.append(StateMachine(self.task, states_kid_1))
        new_population.append(StateMachine(self.task, states_kid_2))
        new_population.append(best_machine)
        new_population.append(StateMachine(self.task, random_mutation(best_machine.states, self.n_states)))
        for _ in range(0, self.n_machines - 4, 2):
            machine1, machine2 = np.random.choice(self.n_machines, size=2, replace=False)
            states_kid_1, states_kid_2 = cross_machines(self.machines[machine1].states, self.machines[machine2].states)
            new_population.append(StateMachine(self.task, states_kid_1))
            new_population.append(StateMachine(self.task, states_kid_2))
        self.machines = new_population
