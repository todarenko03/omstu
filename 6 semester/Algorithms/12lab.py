import time
import random
import string
import numpy as np
import matplotlib.pyplot as plt

class Node():
    def __init__(self, data):
        self.data = data
        self.left = self.right = None

    def add_children(self, left, right):
        self.left = left
        self.right = right


class MobileDB():
    def __init__(self, init_list):
        self.init_list = sorted(init_list, key=lambda x: x[0])
        self.root = self.build_tree()
        self.tarifs_by_popularity = {}
        self.build_demanded_tariff(self.root)

    def build_tree(self):
        self.root = Node(self.init_list[len(self.init_list) // 2])
        self.init_list.pop(len(self.init_list) // 2)
        tempNode = self.root
        for element in self.init_list:
            tempNode = self.root
            while tempNode != None:
                if element < tempNode.data:
                    if tempNode.left == None:
                        tempNode.left = Node(element)
                        break
                    else:
                        tempNode = tempNode.left
                else:
                    if tempNode.right == None:
                        tempNode.right = Node(element)
                        break
                    else:
                        tempNode = tempNode.right
        return self.root



    def print_db(self, node: Node):
        if node == None:
            return
        self.print_db(node.left)
        print(node.data)
        self.print_db(node.right)
    
    def find_by_number(self, node: Node, number: string):
        if node == None:
            return None
        if node.data[0] == number:
            return node.data
        left = self.find_by_number(node.left, number)
        right = self.find_by_number(node.right, number)
        if left != None:
            return left
        if right != None:
            return right
    
    def build_demanded_tariff(self, node):
        if node == None:
            return
        self.build_demanded_tariff(node.left)
        if node.data[2] not in self.tarifs_by_popularity:
            self.tarifs_by_popularity[node.data[2]] = 1
        self.tarifs_by_popularity[node.data[2]] += 1
        self.build_demanded_tariff(node.right)

    def demanded_tariff(self):
        return sorted(self.tarifs_by_popularity, key=self.tarifs_by_popularity.get, reverse=True)[0]

init_list = [
    ['7', 'Николай', 'базовый'],
    ['8', 'Иван', 'максимальный'],
    ['6', 'Максим', 'базовый'],
    ['9', 'Елена', 'базовый'],
    ['1', 'Сергей', 'минимальный']
]

db = MobileDB(init_list)

db.print_db(db.root)
print()
print(db.find_by_number(db.root, "8"))
print()
print(db.demanded_tariff())

times1 = []
times2 = []
times3 = []
x = [i for i in range(100, 3000, 300)]
for i in x:
    el1 = [random.randint(0, 1000) for _ in range(i)]
    el2 = [random.choice(string.ascii_lowercase) for _ in range(i)]
    el3 = [random.choice(['base', 'max', 'min', 'mean', 'ultra']) for _ in range(i)]
    data = []
    for index in range(i):
        data.append([el1[index], el2[index], el3[index]])
    print(i)
    start1 = time.time()
    db = MobileDB(data)
    finish1 = time.time()
    times1.append(finish1 - start1)
    start2 = time.time()
    db.find_by_number(db.root, random.choice(el1))
    finish2 = time.time()
    times2.append(finish2 - start2)
    start3 = time.time()
    db.demanded_tariff()
    finish3 = time.time()
    times3.append(finish3 - start3)

plt.plot(x, times1)
plt.plot(x, times2)
plt.plot(x, times3)
plt.legend(['Build tree', 'Find by number', 'Demanded tariff'])
plt.xlabel = "size"
plt.ylabel = "time"
plt.show()
