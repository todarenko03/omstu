import { Input, Component } from "@angular/core";
import { CountData } from "./count-data.service";

@Component({
    selector: 'counter-3',
    standalone: true,
    templateUrl: './counter3.component.html'
})
export class Counter3{
    @Input() count: number = 0;
    constructor(private countData: CountData) {
        this.countData.currentCount.subscribe(count => this.count = count);
    }
}
