import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountData {
    private countSource = new BehaviorSubject<number>(0);
    currentCount = this.countSource.asObservable();

    getCount(): Observable<number> {
        return this.currentCount;
    }

    changeCount(value: number) {
        this.countSource.next(value);
    }
}
