import random
import time
import matplotlib.pyplot as plt

min_val = -200
max_val = 200

min_size = 100
max_size = 10000

def gnome_sort(arr):
    start_time = time.time()
    i = 1
    count = 0
    for i in range(1, len(arr)):
        j = i
        count += 2
        while arr[j] < arr[j - 1] and j > 0:
            temp = arr[j]
            arr[j] = arr[j - 1]
            arr[j - 1] = temp
            j -= 1
            count += 5
    end_time = time.time()
    return arr, count, (end_time - start_time)

arr = [3, 1, 4, 3, 3, 100, 9]
print(gnome_sort(arr))


def bubble_sort(arr):
    start_time = time.time()
    count = 0
    for i in range(1, len(arr)):
        is_sorted = True
        count += 2
        for j in range(1, len(arr)):
            count += 2
            if arr[j] < arr[j - 1]:
                is_sorted = False
                temp = arr[j]
                arr[j] = arr[j - 1]
                arr[j - 1] = temp
                count += 4
        count += 1
        if is_sorted:
            end_time = time.time()
            return arr, count, (end_time - start_time)
    end_time = time.time()
    return arr, count, (end_time - start_time)

arr = [3, 1, 4, 3, 3, 100, 9]
print(bubble_sort(arr)) 

def create_arr(size, min_val, max_val):
    return [random.randint(min_val, max_val) for i in range(size)]

gnome_counts = []
gnome_times = []
bubble_counts = []
bubble_times = []

for i in range(min_size, max_size, 100):
    arr1 = create_arr(i, min_val, max_val)
    arr2 = create_arr(i, min_val, max_val)

    gnome_res = gnome_sort(arr1)
    bubble_res = bubble_sort(arr2)

    gnome_counts.append(gnome_res[1])
    gnome_times.append(gnome_res[2])

    bubble_counts.append(bubble_res[1])
    # print('g ', gnome_res)
    # print('b ', bubble_res)
    bubble_times.append(bubble_res[2])

sizes = [i for i in range(min_size, max_size, 100)]
plt.plot(sizes, gnome_counts)
plt.plot(sizes, bubble_counts)
plt.xlabel('Размер')
plt.ylabel('Количество операций')
plt.legend(['Гномья', 'Пузырьком'])
plt.show()

plt.plot(sizes, gnome_times)
plt.plot(sizes, bubble_times)
plt.xlabel('Размер')
plt.ylabel('Время выполнения')
plt.legend(['Гномья', 'Пузырьком'])
plt.show()
