const stackContainer = document.getElementById('stack-container');

function renderStack(stack) {
    stackContainer.innerHTML = '';
    stack.forEach((item, index) => {
        const stackItem = document.createElement('div');
        stackItem.className = 'stack-item';
        stackItem.textContent = item;
        stackContainer.appendChild(stackItem);        
    });
}
const stack = [1, 2, 3];

function enqueue() {
    stack.push(stack.length + 1);
    renderStack(stack);
}

function dequeue() {
    if (stack.length > 0) {
        stack.pop(stack.length);
        renderStack(stack);
    } 
}

renderStack(stack);
