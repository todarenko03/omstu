import streamlit as st
from evolution import Evolution
from test import matrix
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

def main():
    st.title("Simulated ant")
    n_machines = st.sidebar.slider("Number of machines", min_value=1, max_value=500, value=50)
    n_states = st.sidebar.slider("Number of states", min_value=1, max_value=500, value=20)
    n_epochs = st.sidebar.slider("Number of epochs", min_value=1, max_value=1000, value=100)
    
    calculating_matrix = matrix
    
    paste = st.sidebar.checkbox("Paste own matrix")
    
    if paste:
        matrix_size = st.number_input("Matrix size: ", value=10, placeholder="type a number...")
        calculating_matrix = [[0 for i in range(matrix_size)] for j in range(matrix_size)]
        for i in range(matrix_size):
            for j in range(matrix_size):
                calculating_matrix[i][j] = st.number_input(f"Is food in {i}:{j}: ", value=0, min_value=0, max_value=1, key=i * 100 + j)
        
        
    if st.sidebar.button("Calculate"):
        evol = Evolution(calculating_matrix, n_machines=n_machines, n_states=n_states)
        for _ in range(n_epochs):
            evol.epoch()
            total_food = 0
        for row in matrix:
            for element in row:
                total_food += element
        st.write("Eated food: ", evol.best_score)
        st.write("Total food: ", total_food)
        st.write("The best fsm: ", evol.best_machine.states)
        fig, ax = plt.subplots()
        res_matr = np.zeros((len(matrix), len(matrix)))
        for road in evol.best_road:
            if road[2] == 1 or res_matr[road[0], road[1]] == 2:
                res_matr[road[0], road[1]] = 2
            else:
                res_matr[road[0], road[1]] = 1
        sns.heatmap(res_matr, ax=ax)
        st.write(fig)

if __name__ == '__main__':
    main()
