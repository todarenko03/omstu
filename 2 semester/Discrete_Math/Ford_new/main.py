class Graph:
    def __init__(self, nodes):
        self.nodes = nodes
        self.graph = []

    def add_edge(self, u, v, w):
        self.graph.append([u, v, w])

    def show(self, road):
        print(road)

    def ford(self, start):
        d = [float('inf') for x in range(self.nodes)]
        d[start] = 0

        for i in range(self.nodes - 1):
            for u, v, w in self.graph:
                if d[v] > d[u] + w:
                    d[v] = d[u] + w

        self.show(d)


n = int(input())
graph = Graph(n)

for i in range(n):
    u, v, w = map(int, input().split())
    graph.add_edge(u, v, w)

start = int(input())
graph.ford(start)
