import { Component } from "@angular/core";

@Component({
    selector: 'app-counter',
    standalone: true,
    templateUrl: 'counter.component.html',
})
export class CounterComponent {
    count: number = 0;

    increment() {
        this.count++;
    }

    decrement() {
        this.count--;
  }
}