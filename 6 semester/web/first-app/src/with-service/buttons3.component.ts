import { Component } from "@angular/core";
import { Counter3 } from "./counter3.component";
import { CountData } from "./count-data.service";

@Component({
    selector: "buttons-3",
    standalone: true,
    imports: [Counter3],
    templateUrl: 'buttons3.component.html'
})
export class Buttons3 {
    count: number = 0;

    constructor(private countData: CountData) {
        this.countData.currentCount.subscribe(count => this.count = count);
    }

    increment() {
        this.count++;
        this.countData.changeCount(this.count);
    }

    decrement() {
        this.count--;
        this.countData.changeCount(this.count);
  }
}
