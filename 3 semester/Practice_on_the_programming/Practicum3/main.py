import numpy as np
from scipy import linalg, stats

M = np.array([[2, 0, -1, 25],
              [-6, 28, 0, -7.4],
              [1, -5, 13, 2.8],
              [0, 4, 3, 1.7]])

print('#1')

print(M)

print('#2')

df = linalg.lu(M)
L = df[1]
U = df[2]
print(L)
print(U)

print('#3')

print(linalg.det(L))
print(linalg.det(U))

print('#4')

sample1 = stats.norm.rvs(size=100)
print(sample1)
sample2 = stats.uniform.rvs(size=100)
print(sample2)

print('#5')

print(np.mean(sample1), '\t', np.mean(sample2))
print(stats.mode(sample1, keepdims=True), '\t', stats.mode(sample2, keepdims=True))
print(np.median(sample1), '\t', np.median(sample2))
print(min(sample1), '\t', min(sample2))
print(max(sample1), '\t', max(sample2))
print(np.std(sample1), '\t', np.std(sample2))

print('#6')

print(stats.chisquare(sample1))
print(stats.chisquare(sample2))
