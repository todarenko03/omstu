const queueContainer = document.getElementById('queue-container');

function renderQueue(queue) {
    queueContainer.innerHTML = '';
    queue.forEach((item, index) => {
        const queueItem = document.createElement('div');
        queueItem.className = 'queue-item';
        queueItem.textContent = item;
        queueContainer.appendChild(queueItem);        
    });
}
const queue = [1, 2, 3];

function enqueue() {
    queue.push(queue.length + 1);
    renderQueue(queue);
}

function dequeue() {
    if (queue.length > 0) {
        queue.shift();
        renderQueue(queue);
    } 
}

renderQueue(queue);
