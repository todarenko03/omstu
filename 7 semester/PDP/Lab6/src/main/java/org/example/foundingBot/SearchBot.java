package org.example.foundingBot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SearchBot implements ISearchBot {

    private final List<File> javaFiles;

    private final Pattern patternClass = Pattern.compile("class\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternInterface = Pattern.compile("interface\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassExtends = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassImplements = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassImplementsAfterExtends = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassExtendsAfterImplements = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)");

    private final BlockingQueue<File> tasks;
    private final BlockingQueue<Future<Map<String, List<String>>>> results;

    public SearchBot(
            final String folderName
    ) throws InterruptedException {
        this.tasks = new LinkedBlockingQueue<File>();
        this.results = new LinkedBlockingQueue<Future<Map<String, List<String>>>>();
        Path folderPath = Paths.get(folderName);
        this.javaFiles = collectJavaFiles(folderPath);
        for (File file: javaFiles) {
            tasks.put(file);
        }
    }


    public void search(final Map<String, List<String>> hierarchy) throws InterruptedException, ExecutionException {

            ExecutorService executorService = Executors.newFixedThreadPool(12);

            for (int i = 0; i < javaFiles.size(); i++) {
                results.add(executorService.submit(() -> processFile(tasks.take())));
            }

            for (int i = 0; i < javaFiles.size(); i++) {
                results.take().get().forEach((key, values) -> {
                    if (hierarchy.containsKey(key)) {
                        hierarchy.get(key).addAll(values);
                    } else {
                        hierarchy.put(key, new ArrayList<>(values));
                    }
                });
            }

            executorService.shutdown();
    };

    private List<File> collectJavaFiles(Path projectPath) {
        try (Stream<Path> walk = Files.walk(projectPath)) {
            return walk.filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".java"))
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private Map<String, List<String>> processFile(File file) throws IOException {
        String text = new String(Files.readAllBytes(file.toPath()));

        Map<String, List<String>> map = new HashMap<>();

        Stream<MatchResult> patternClassResults = patternClass.matcher(text).results();
        Stream<MatchResult> patternInterfaceResults = patternInterface.matcher(text).results();
        Stream<MatchResult> patternClassExtendsResults = patternClassExtends.matcher(text).results();
        Stream<MatchResult> patternClassImplementsResults = patternClassImplements.matcher(text).results();
        Stream<MatchResult> patternClassImplementsAfterExtendsResults = patternClassImplementsAfterExtends.matcher(text).results();
        Stream<MatchResult> patternClassExtendsAfterImplementsResults = patternClassExtendsAfterImplements.matcher(text).results();

        patternClassResults.forEach(
                res -> {
                    map.put(res.group(1), map.getOrDefault(res.group(1), new ArrayList<>()));
                });
        patternInterfaceResults.forEach(
                res -> {
                    map.put(res.group(1), map.getOrDefault(res.group(1), new ArrayList<>()));
                });
        patternClassExtendsResults.forEach(
                res -> {
                    List<String> list = map.getOrDefault(res.group(2), new ArrayList<>());
                    list.add(res.group(1));
                    map.put(res.group(2), list);
                }
        );
        patternClassImplementsResults.forEach(
                res -> {
                    List<String> list = map.getOrDefault(res.group(2), new ArrayList<>());
                    list.add(res.group(1));
                    map.put(res.group(2), list);
                }
        );
        patternClassImplementsAfterExtendsResults.forEach(
                res -> {
                    List<String> list = map.getOrDefault(res.group(3), new ArrayList<>());
                    list.add(res.group(1));
                    map.put(res.group(3), list);
                }
        );
        patternClassExtendsAfterImplementsResults.forEach(
                res -> {
                    List<String> list = map.getOrDefault(res.group(3), new ArrayList<>());
                    list.add(res.group(1));
                    map.put(res.group(3), list);
                }
        );

        return map;
    }
}
