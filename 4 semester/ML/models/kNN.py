import numpy as np

def euclidean_distance(x1, x2):
    return np.sqrt(np.sum((x1 - x2) ** 2, axis=1))

class KNeighborsClassifier:
    def __init__(self, k=5):
        self.X = np.array([])
        self.y = np.array([])
        self.k = k
        
    def fit(self, X_train, y_train):
        self.X = np.array(X_train)
        self.y = np.array(y_train)
        
    def predict(self, X_test):
        indices = np.random.choice(len(self.X), size=5000)
        self.X = self.X[indices]
        if X_test.shape[0] < self.X.shape[0]:
            indices = np.random.choice(len(self.X), size=len(X_test))
            self.X = self.X[indices]
        neighbors = []
        distance = [euclidean_distance(X_test, self.X) for X_test in X_test]
        for row in distance:
            enum_row = enumerate(row)
            top_dist = sorted(enum_row, key=lambda x: x[1])[:self.k]
            n_indicies = np.array([x[0] for x in top_dist])
            ans = np.argmax(np.bincount(self.y[n_indicies]))
            neighbors.append(ans)
        return np.array(neighbors)
        