from transformers import AutoTokenizer, AutoModelForSequenceClassification

tokenizer = AutoTokenizer.from_pretrained("Hello-SimpleAI/chatgpt-detector-roberta")
model = AutoModelForSequenceClassification.from_pretrained("Hello-SimpleAI/chatgpt-detector-roberta")

def predict(data):
    model_inputs = tokenizer(data.text, return_tensors='pt')
    outputs = model(**model_inputs)
    return result_interpretation(outputs.logits.tolist())

def result_interpretation(data):
    if data[0][0] < data[0][1]:
        return "Текст написан чатом GPT"
    else:
        return "Текст написан человеком"
