import numpy as np

my_array = np.array([x for x in range(10, 70, 2)])
print(my_array)
print()

print('#2')

A = np.reshape(my_array, (6, 5)).T
print(A)
print()

print('#3')

A = A * 2.5 - 5
print(A)
print()

print('#4')

B = np.random.uniform(0, 10, (6, 3))
print(B)
print()

print('#5')

a = np.sum(A, axis=1)
print(a)
b = np.sum(A, axis=0)
print(b)
print()

print('#6')

print(np.dot(A, B))
print()

print('#7')

A = np.delete(A, 2, axis=1)
B = B.transpose()
B = np.concatenate((B, (np.random.randint(10, 20, (3, 6)))))
B = B.transpose()
print(A)
print(B)
print()

print('#8')

Adet = np.linalg.det(A)
Bdet = np.linalg.det(B)
print(Adet)
print(Bdet)
if Adet != 0:
    print(np.linalg.inv(A))
if Bdet != 0:
    print(np.linalg.inv(B))
print()

print('#9')

print(np.linalg.matrix_power(A, 6))
print(np.linalg.matrix_power(B, 14))
print()

print('#10')

M = np.array([[2, 0, -1, 25, 6.7],
              [-6, 28, 0, -7.4, -4],
              [1, -5, 13, 2.8, 16],
              [0, 4, 3, 1.7, 8]])


def gauss(Matr):
    for nrow, row in enumerate(Matr):
        diagonal = row[nrow]
        row /= diagonal

        for lower_row in Matr[nrow+1:]:
            el = lower_row[nrow]
            lower_row -= el * row

    for nrow in range(len(Matr) - 1, 0, -1):
        row = Matr[nrow]

        for upper_row in Matr[:nrow]:
            el = upper_row[nrow]
            upper_row -= el * row

    mlen1 = len(Matr)
    Matr = np.transpose(Matr)
    mlen2 = len(Matr[0])

    for i in range(mlen1):
        print(Matr[mlen2][i])


gauss(M)
