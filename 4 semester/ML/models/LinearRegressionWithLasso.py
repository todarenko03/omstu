import numpy as np 
from models.metrics import R2
class LinearRegressionWithLasso:
    def __init__(self, l):
        self.w1 = 0
        self.w2 = 0
        self.lyambda = l
        self.R2 = 0
    def fit(self, X, y):
        X = np.array(X)
        y = np.array(y)
        X = np.concatenate((np.ones((X.shape[0], 1)), X), axis=1)
        w = np.dot(np.dot(np.linalg.inv(np.dot(X.T, X) + self.lyambda*np.eye(X.shape[1])), X.T), y)
        self.w1 = w[0]
        self.w2 = w[1:]
    def predict(self, X):
        X = np.array(X)
        return np.dot(X, self.w2) + self.w1
    def score(self, X, y):
        y_pred = self.predict(X)
        return R2(y, y_pred)

