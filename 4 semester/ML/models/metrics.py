import numpy as np
from sklearn.metrics import confusion_matrix

def MAE(y_test, y_pred):
    return np.mean(np.abs(y_test - y_pred))
    
def MAPE(y_test, y_pred):
    return np.mean(np.abs(y_test - y_pred)/y_test) 

def MSE(y_test, y_pred):
    return np.mean(np.power((y_test-y_pred), 2))

def RMSE(y_test, y_pred):
    return np.mean(np.sqrt(np.power((y_test-y_pred), 2)))

def R2(y_test, y_pred):
    sse = ((y_test - y_pred)**2).sum(axis=0)
    tse = ((y_test - np.average(y_test, axis=0)) ** 2).sum(axis = 0)
    return 1 - (sse / tse)
###

def Accuracy(y_test, y_pred):
    cm = confusion_matrix(y_test, y_pred)
    cm = np.flip(cm)
    answer = (cm[0][0] + cm[1][1]) / (cm[0][0] + cm[1][1] + cm[0][1] + cm[1][0])
    return answer

def Precision(y_test, y_pred):
    cm = confusion_matrix(y_test, y_pred)
    cm = np.flip(cm)
    answer = cm[0][0] / (cm[0][0] + cm[0][1])
    return answer

def Recall(y_test, y_pred):
    cm = confusion_matrix(y_test, y_pred)
    cm = np.flip(cm)
    answer = cm[0][0] / (cm[0][0] + cm[1][0])
    return answer

def F1(y_test, y_pred):
    p = Precision(y_test, y_pred)
    r = Recall(y_test, y_pred)
    answer = 2 * ((p * r) / (p + r))
    return answer
