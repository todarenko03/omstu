import { Component } from "@angular/core";
import { Counter2 } from "./counter2.component";

@Component({
    selector: "buttons-2",
    standalone: true,
    imports: [Counter2],
    templateUrl: 'buttons2.component.html'
})
export class Buttons2 {
    count: number = 0;

    increment() {
        this.count++;
    }

    decrement() {
        this.count--;
  }
}