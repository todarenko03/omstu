import random
import numpy as np
import plotly.graph_objects as go


def generate_floorplanning_problem(num_blocks, chip_size, min_block_size, max_block_size):
    block_sizes = [(random.randint(min_block_size, max_block_size), random.randint(min_block_size, max_block_size)) for i in range(num_blocks)] 

    for block_size in block_sizes:
        if block_size[0] > chip_size or block_size[1] > chip_size:
            print("Ошибка: Размер блока превышает размер микросхемы.")
            return None, None, None

    connections = np.zeros((num_blocks, num_blocks))
    for i in range(num_blocks):
        for j in range(i+1, num_blocks):
            if random.random() < 0.5:
                connections[i][j] = connections[j][i] = 1

    return chip_size, block_sizes, connections


def check_valid_genome(genome, chip_size, block_sizes, connections):
    occupied_positions = set()
    chip = [[0] * chip_size for i in range(chip_size)]

    for block_idx in genome:
        block_size = block_sizes[block_idx]
        position_found = False

        if max(block_size) > chip_size:
            return False

        position_found = any(
            all(chip[row + i][col + j] == 0 for i in range(block_size[0]) for j in range(block_size[1]))
            and (row + block_size[0] <= col + chip_size)
            and (row + col <= chip_size - 1)
            for row in range(chip_size - block_size[0] + 1)
            for col in range(chip_size - block_size[1] + 1)
        )

        if not position_found:
            return False

        for i in range(block_size[0]):
            for j in range(block_size[1]):
                chip[row + i][col + j] = 1

    for i in range(len(genome)):
        for j in range(i+1, len(genome)):
            if connections[genome[i]][genome[j]] == 1:
                if not are_blocks_adjacent(genome[i], genome[j], chip_size, block_sizes, genome):
                    return False

    return True


def are_blocks_adjacent(block1, block2, chip_size, block_sizes, genome):
    block1_position = genome.index(block1)
    block2_position = genome.index(block2)
    block1_size = block_sizes[block1]
    block2_size = block_sizes[block2]

    if block1_position + block1_size[1] <= block2_position or block2_position + block2_size[1] <= block1_position:
        return False

    if block1_position + block1_size[0] <= block2_position or block2_position + block2_size[0] <= block1_position:
        return False

    return True


def generate_population(size, num_blocks, chip_size, block_sizes):
    population = []
    for _ in range(size):
        genome = random.sample(range(num_blocks), num_blocks)
        random.shuffle(genome)
        population.append(genome)
    return population


def calculate_fitness(genome, chip_size, block_sizes, connections):

    adjacency_fitness = calculate_adjacency_fitness(genome, connections)

    return adjacency_fitness

def calculate_adjacency_fitness(genome, connections):
    adjacency = 0
    for i in range(len(genome) - 1):
        if type(genome[i]) == list or type(genome[i + 1]) == list:
            continue
        if connections[int(genome[i]), int(genome[i + 1])] == 1:
            adjacency += 1
    return adjacency


def crossover(parents, chip_size):
    p1, p2 = parents
    size = len(p1)
    point = random.randint(1, size - 1)
    child1 = p1[:point]
    child2 = p2[:point]

    for i in range(size):
        if p2[i] not in child1 and len(child1) < chip_size:
            child1.append(p2[i])
        if p1[i] not in child2 and len(child2) < chip_size:
            child2.append(p1[i])

    return child1, child2


def mutation(genome, mutation_rate):
    mutated_genome = genome[:]
    if random.random() < mutation_rate:
        idx1, idx2 = random.choices(range(len(mutated_genome)), k=2)
        mutated_genome[idx1], mutated_genome[idx2] = mutated_genome[idx2], mutated_genome[idx1]

    idx = 0
    while idx < len(mutated_genome):
        if mutated_genome[idx] not in mutated_genome[:idx]:
            idx += 1
        else:
            block_idx = mutated_genome[idx]
            next_idx = next(i for i, block in enumerate(mutated_genome[idx + 1:]) if block != block_idx)
            next_idx += idx + 1
            mutated_genome[idx + 1], mutated_genome[next_idx] = mutated_genome[next_idx], mutated_genome[idx + 1]
            idx += 1

    return mutated_genome


def selection(population, fitness_values, k):
    selected_indices = []
    while len(selected_indices) < k:
        tournament_indices = random.choices(range(len(population)), k=2)
        tournament_fitness = [fitness_values[i] for i in tournament_indices]
        winner_index = tournament_indices[tournament_fitness.index(max(tournament_fitness))]
        selected_indices.append(winner_index)
    return [population[idx] for idx in selected_indices]


def floorplanning_genetic_algorithm(num_blocks, chip_size, block_sizes, connections, population_size=100,
                                    num_generations=100, mutation_rate=0.1, elitism_ratio=0.01):
    population = generate_population(population_size, num_blocks, chip_size, block_sizes)
    best_fitness = 0
    best_genome = None

    for generation in range(num_generations):
        fitness_values = [calculate_fitness(genome, chip_size, block_sizes, connections) for genome in population]
        population = [x for _, x in sorted(zip(fitness_values, population), reverse=True)]
        fitness_values = sorted(fitness_values, reverse=True)

        if fitness_values[0] > best_fitness:
            best_fitness = fitness_values[0]
            best_genome = population[0]

        elitism_count = int(population_size * elitism_ratio)
        elite_population = population[:elitism_count]

        children = []
        while len(children) < population_size - elitism_count:
            parent1, parent2 = random.choices(population[:elitism_count], k=2)
            child1, child2 = crossover((parent1, parent2), chip_size)
            children.append(child1)
            children.append(child2)

        mutated_population = [mutation(child, mutation_rate) for child in children]

        population = elite_population + mutated_population

    return best_genome


def visualize_floorplan(genome, fitness, chip_size, block_sizes):
    chip = [[0] * chip_size for i in range(chip_size)]
    block_colors = {}

    block_sizes = [(size[1], size[0]) for size in block_sizes]

    palette = []
    for i in range(len(genome)):
        color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        hex_color = '#%02x%02x%02x' % color
        palette.append(hex_color)
 
    for idx, block_idx in enumerate(genome):
        block_size = block_sizes[block_idx]
        position_found = False

        for row in range(chip_size - block_size[0] + 1):
            for col in range(chip_size - block_size[1] + 1):
                if all(chip[row + i][col + j] == 0 for i in range(block_size[0]) for j in range(block_size[1])) and (
                        row + block_size[0] <= chip_size) and (col + block_size[1] <= chip_size):
                    position_found = True
                    break
            if position_found:
                break

        if not position_found:
            row = 0
            col = 0
            while any(chip[row + i][col + j] != 0 for i in range(block_size[0]) for j in range(block_size[1])):
                col += 1
                if col >= chip_size - block_size[1] + 1:
                    col = 0
                    row += 1
                if row >= chip_size - block_size[0] + 1:
                    print("Ошибка: Блоки не могут быть размещены на микросхеме.")
                    return None, None, None

        block_color = palette[block_idx]
        block_colors[block_idx] = block_color

        for i in range(block_size[0]):
            for j in range(block_size[1]):
                chip[row + i][col + j] = block_idx + 1

    data = []
    for row in range(chip_size):
        for col in range(chip_size):
            block_idx = chip[row][col]
            if block_idx != 0:
                block_color = block_colors[block_idx - 1]
                data.append(
                    go.Scatter(
                        x=[col, col + 1, col + 1, col, col],
                        y=[chip_size - row - 1, chip_size - row - 1, chip_size - row, chip_size - row, chip_size - row - 1],
                        mode="lines",
                        line=dict(width=2, color=block_color),
                        fill="toself",
                        fillcolor=block_color,
                        name=str(block_idx - 1),  
                    )
                )

    layout = go.Layout(
        title="Floorplan",
        xaxis=dict(range=[-0.5, chip_size - 0.5], autorange=False),
        yaxis=dict(range=[-0.5, chip_size - 0.5], autorange=False),
        showlegend=False,
        width=600,
        height=600,
    )

    print("[+] Лучший геном: ", genome)
    print("[+] Значение приспособленности: ", fitness)
    
    fig = go.Figure(data=data, layout=layout)
    return fig, genome, fitness

# num_blocks = 5
# chip_size = 10
# min_block_size = 1
# max_block_size = 5

# chip_size, block_sizes, connections = generate_floorplanning_problem(num_blocks, chip_size, min_block_size, max_block_size)
# if chip_size is None:
#     print("Не удалось сгенерировать задачу о раскладке микросхемы.")
# else:
#     best_genome = floorplanning_genetic_algorithm(num_blocks, chip_size, block_sizes, connections)
#     best_fitness = calculate_fitness(best_genome, chip_size, block_sizes, connections)
#     print('Размеры блоков: ', block_sizes)
#     print('Матрица связей: ','\n', connections)
#     visualize_floorplan(best_genome, best_fitness, chip_size, block_sizes)
