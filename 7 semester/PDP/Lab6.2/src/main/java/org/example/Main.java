package org.example;

import org.example.foundingBot.SearchBot;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Map<String, List<String>> hierarchy = new HashMap<>();

        String directory = "/home/todarenko03/Downloads/spring-boot-main (1)";

        SearchBot searchBot = new SearchBot(directory);
        searchBot.search(hierarchy);
        hierarchy.forEach((key, value) -> {
            if (!value.isEmpty()) {
                System.out.println(key + ": " + value.toString());
            }
        });
    }
}
