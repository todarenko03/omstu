import time
import random
import matplotlib.pyplot as plt

def opt_schedule(m: int, orders: list[tuple[any, int, int]]) -> list[int]:
    orders.sort(key=lambda x: x[2], reverse=True)
    result = [0 for i in range(m)]
    for order in orders:
        for i in range(order[1], -1, -1):
            if result[i] == 0:
                result[i] = order[0]
                break
    return result

orders = [('A', 1, 40), ('B', 0, 25), ('C', 1, 30), ('D', 0, 15), ('E', 2, 20)]

print(opt_schedule(3, orders))

def division_into_groups(m: int, children: list[tuple[any, int]]) -> int:
    children.sort(reverse=True)

    count = 0
    prev_age = 1e+3

    for child in children:
        if prev_age - child[1] > m:
            count += 1
        prev_age = child[1]
    
    return count

children = [(1, 2), (2, 3), (3, 6), (4, 2), (5, 10)]

print(division_into_groups(2, children))


times1 = []
times2 = []
x = [i for i in range(100, 10000, 100)]
for i in x:
    start1 = time.time()
    opt_schedule(i // 10, [(j, random.randint(0, (i // 10) - 1), random.randint(j // 2, i * 2)) for j in range(10, i + i // 3)])
    finish1 = time.time()
    times1.append(finish1 - start1)
    start2 = time.time()
    division_into_groups(2, [(j, random.randint(0, j * 2)) for j in range(10, 2 * i)])
    finish2 = time.time()
    times2.append(finish2 - start2)

plt.plot(x, times1)
plt.plot(x, times2)
plt.xlabel = "size"
plt.ylabel = "time"
plt.legend(["opt_schedule", "division_into_groups"])
plt.show()
