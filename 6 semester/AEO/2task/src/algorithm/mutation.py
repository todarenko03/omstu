import numpy as np


def random_mutation(machine, n_states):
    state = np.random.randint(low=0, high=n_states)
    move0, move1 = np.random.randint(low=0, high=3, size=2)
    available_states = np.delete(np.arange(0, n_states), state)
    state0, state1 = np.random.choice(available_states, size=2)
    machine[state] = np.array([move0, state0, move1, state1])
    return machine
