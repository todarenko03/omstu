import torch
import torch.nn as nn

class Encoder(nn.Module):
    def __init__(self, input_size, hidden_size, dropout_k=0.01):
        super(Encoder, self).__init__()
        self.hidden_size = hidden_size

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, batch_first=True)
        self.dropout = nn.Dropout(dropout_k)
        
    def forward(self, input):
        embedded = self.dropout(self.embedding(input))
        output, hidden = self.gru(embedded)
        
        return output, hidden

in_size = 5
hidden_size = 16

encoder = Encoder(input_size=in_size, hidden_size=hidden_size)

data = torch.randint(0, in_size, (1, 8), dtype=torch.long)

out, _ = encoder(data)
print(data)
print(out)
