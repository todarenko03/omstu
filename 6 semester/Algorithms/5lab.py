import time
import random
import matplotlib.pyplot as plt

def gray_permutations(n: int):
    for i in range(1, n**2):
        vector = []
        for j in range(1, n + 1):
            if (i) % (4 * j) >= 2 ** (j - 1) + 1 \
            and (i) % (4 * j) <= 2 ** (j - 1) + 2 ** j:
                vector.append(1)
            else:
                vector.append(0)
        vector.reverse()
        print(vector)

gray_permutations(4)

times = []
ns = [i for i in range(5, 20, 2)]

for i in ns:
    start = time.time()
    gray_permutations(i)
    finish = time.time()
    times.append(finish - start)

plt.plot(ns, times)
plt.xlabel("ns")
plt.ylabel("time")
plt.show()

def pseudo_random_shuffle1(arr: list[int]) -> list[int]:
    newArr = []
    for i in range(len(arr) - 1, -1, -1):
        index = random.randint(0, i)
        el = arr.pop(index)
        newArr.append(el)
    return newArr

arr = [i for i in range(1, 9)]

print(pseudo_random_shuffle1(arr))

arr = [i for i in range(1, 9)]

def pseudo_random_shuffle2(arr: list[int]) -> list[int]:
    for i in range(len(arr) - 1, -1, -1):
        index = random.randint(0, i)
        arr[i], arr[index] = arr[index], arr[i]
    return arr

print(pseudo_random_shuffle2(arr))

times1 = []
times2 = []
ns = [i for i in range(10000, 100000, 1000)]

for i in ns:
    start1 = time.time()
    pseudo_random_shuffle1([j for j in range(1, i)])
    finish1 = time.time()
    times1.append(finish1 - start1)
    start2 = time.time()
    pseudo_random_shuffle2([j for j in range(1, i)])
    finish2 = time.time()
    times2.append(finish2 - start2)

plt.plot(ns, times1)
plt.plot(ns, times2)
plt.xlabel("ns")
plt.ylabel("time")
plt.legend(['first', 'second'])
plt.show()
