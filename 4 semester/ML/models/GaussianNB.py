import numpy as np

class GaussianNB:
    def __init__(self):
        self.X_train = 0
        self.y_train = 0

    def fit(self, X_train, y_train):
        self.X_train = X_train
        self.y_train = y_train
        
        self.n_samples, self.n_features = X_train.shape
        self.n_classes = len(np.unique(self.y_train))
        
        self.mean = np.zeros((self.n_classes, self.n_features))
        self.variance = np.zeros((self.n_classes, self.n_features))
        self.priors = np.zeros((self.n_classes))
        
        for c in range(self.n_classes):
            X_c = self.X_train[y_train == c]
            
            self.mean[c, :] = np.mean(X_c, axis=0)
            self.variance[c, :] = np.var(X_c, axis=0)
            self.priors[c] = X_c.shape[0] / self.n_samples
        
    def gaussian_density(self, x, mean, var):
        ans = (1 / np.sqrt(np.pi * 2 * var)) * (np.exp(-0.5 * ((x - mean) ** 2) / var))
        return ans
    
    def get_class_probability(self, x):
        posteriors = []
        for c in range(self.n_classes):
            mean = self.mean[c]
            variance = self.variance[c]
            prior = np.log(self.priors[c])
            
            posterior = np.sum(np.log(self.gaussian_density(np.array(x), mean, variance)))
            posterior += prior
            posteriors.append(posterior)
        
        return np.argmax(posteriors)
    
    def predict(self, X_test):
        y_pred = [self.get_class_probability(x) for x in X_test]
        return np.array(y_pred)
        