package org.example;

import org.example.foundingBot.ISearchBot;
import org.example.foundingBot.SearchBot;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Лабораторная работа 2
 * Поисковый робот для исходного текста программы
 * Для Java проекта (локальной папки) построить и напечатать в консоли обратный индекс наследования классов.
 * Для каждого класса необходимо найти (напечатать) классы, для которых он является базовым (родительским).
 * Должны корректно обрабатываться ключевые слова class, interface, extends, implements.
 * Необходимо использовать интерфейс Map, метод getOrDefault(). Желательно использовать Stream API.
 */

public class Main {
    public static void main(String[] args) {
        Map<String, List<String>> hierarchy = new HashMap<>();

        File directory = new File("src/main/java/org/example");

        ISearchBot searchBot = new SearchBot();
        searchBot.search(directory, hierarchy);

        hierarchy.forEach((key, value) -> System.out.println(key + ": " + value.toString()));
    }
}
