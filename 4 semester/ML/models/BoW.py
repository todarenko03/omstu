import pandas as pd
import re
import numpy as np
from collections import Counter
from collections import defaultdict

def preprocess_text(text):
    text = text.lower()
    text = re.sub(r"[^\w\s]", "", text)
    return text

class BoW:

    def fit_transform(self, texts):
        preprocessed_texts = texts.apply(preprocess_text)

        word_counts = Counter()
        for text in preprocessed_texts:
            words = text.split()
            word_counts.update(words)

        word_list = list(word_counts.keys())

        matrix = []
        for text in preprocessed_texts:
            words = text.split()
            row = [words.count(word) for word in word_list]
            matrix.append(row)

        matrix = np.array(matrix, dtype=np.int32)

        df = pd.DataFrame(matrix, columns=word_list)

        return df