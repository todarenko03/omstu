import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'data-table',
  standalone: true,
  imports: [CommonModule],
  template: `
    <table>
      <thead>
        <tr>
          <th>Колонка 1</th>
          <th>Колонка 2</th>
          <th>Колонка 3</th>
          <th>Колонка 4</th>
        </tr>
      </thead>
      <tbody>
        <ng-container *ngIf="data && data.length > 0">
          <tr *ngFor="let row of data; let i = index">
            <td>{{ row.column1 }}</td>
            <td>{{ row.column2 }}</td>
            <td>{{ row.column3 }}</td>
            <td>{{ row.column4 }}</td>
          </tr>
        </ng-container>
        <ng-container *ngIf="!data || data.length === 0">
          <tr>
            <td colspan="4">Нет данных</td>
          </tr>
        </ng-container>
      </tbody>
    </table>
  `
})
export class DataTableComponent {
  @Input()
  data: any[] = [];
}
