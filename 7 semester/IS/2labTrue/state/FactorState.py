from node.BoolNode import BoolNode
from node.NumNode import NumNode
from node.VarNode import VarNode
from state.State import State

class FactorState(State):
    def __init__(self, name):
        super().__init__(name)
        self.handlers = {
            'NUMBER': self.handle_number,
            'ID': self.handle_id,
            'BOOLEAN': self.handle_boolean,
            'LBRACKET': self.handle_parentheses
        }

    def handle(self, fsm, token):
        token_type = token.token_type
        return self.handlers[token_type](fsm, token)

    def handle_number(self, fsm, token):
        node = NumNode(token.value)
        fsm.consume('NUMBER')
        return node

    def handle_id(self, fsm, token):
        node = VarNode(token.value)
        fsm.consume('ID')
        return node

    def handle_boolean(self, fsm, token):
        node = BoolNode(token.value)
        fsm.consume('BOOLEAN')
        return node

    def handle_parentheses(self, fsm, token):
        fsm.consume('LBRACKET')
        node = fsm.expression_state.handle(fsm, fsm.current_token)
        fsm.consume('RBRACKET')
        return node
