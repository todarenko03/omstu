import { Input, Component } from "@angular/core";

@Component({
    selector: 'counter-2',
    standalone: true,
    templateUrl: './counter2.component.html'
})
export class Counter2{
    @Input() count: number = 0;
}
