from state.ExpressionState import ExpressionState
from state.FactorState import FactorState
from state.TermState import TermState


class FSM:
    def __init__(self, tokens):
        self.tokens = tokens
        self.pos = 0
        self.current_token = self.tokens[self.pos]
        self.current_node = None

        self.expression_state = ExpressionState("EXPRESSION")
        self.term_state = TermState("TERM")
        self.factor_state = FactorState("FACTOR")

        self.current_state = self.expression_state

        self.transition_table = {
            ("EXPRESSION", "PLUS"): self.term_state,
            ("EXPRESSION", "MINUS"): self.term_state,
            ("TERM", "MULT"): self.factor_state,
            ("TERM", "DIV"): self.factor_state,
            ("FACTOR", "NUMBER"): self.expression_state,
            ("FACTOR", "ID"): self.expression_state,
            ("FACTOR", "BOOLEAN"): self.expression_state,
            ("FACTOR", "LBRACKET"): self.expression_state,
            ("EXPRESSION", "EOF"): None
        }

    def set_state(self, state):
        self.current_state = state

    def get_transition(self, current_state, token_type):
        return self.transition_table.get((current_state.name, token_type))

    def consume(self, token_type):
        self.pos += 1
        if self.pos < len(self.tokens):
            self.current_token = self.tokens[self.pos]

    def parse(self):
        return self.current_state.handle(self, self.current_token)
    