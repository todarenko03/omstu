# from fsm.FSM import FSM
# from lexer.Lexer import Lexer
# from node.BinOpNode import BinOpNode
# from node.BoolNode import BoolNode
# from node.NumNode import NumNode
# from node.VarNode import VarNode


# def print_tree(node, indent=0):
#     if isinstance(node, BinOpNode):
#         print('  ' * indent + f"BinOp({node.op}):")
#         print('  ' * (indent + 1) + "Left:")
#         print_tree(node.left, indent + 2)
#         print('  ' * (indent + 1) + "Right:")
#         print_tree(node.right, indent + 2)
#     elif isinstance(node, NumNode):
#         print('  ' * indent + f"Number({node.value})")
#     elif isinstance(node, VarNode):
#         print('  ' * indent + f"Variable({node.value})")
#     elif isinstance(node, BoolNode):
#         print('  ' * indent + f"Boolean({node.value})")


# def main():
#     text = input("Выражение: ")
#     lexer = Lexer()
#     tokens = lexer.tokenize(text)
    
#     fsm = FSM(tokens)
#     ast = fsm.parse()
    
#     print("Синтаксическое дерево: ")
#     print_tree(ast, indent=0)


# if __name__ == '__main__':
#     main()
import re
from typing import List, Dict, Tuple

# Класс для представления грамматики
class Grammar:
    def __init__(self, start_symbol: str, non_terminals: List[str], rules: Dict[str, List[List[str]]]):
        self.start_symbol = start_symbol
        self.non_terminals = non_terminals
        self.rules = rules

class LL1Parser:
    def __init__(self, grammar: Grammar, parse_table: Dict[Tuple[str, str], List[str]]):
        self.grammar = grammar
        self.parse_table = parse_table

    def parse(self, input_string: str) -> bool:
        stack = ['$']
        stack.append(self.grammar.start_symbol)
        tokens = self.tokenize(input_string) + ['$']
        pointer = 0

        while stack:
            top = stack.pop()
            current_token = tokens[pointer] if pointer < len(tokens) else '$'

            if top == current_token:
                pointer += 1
            elif top in self.grammar.non_terminals:
                rule = self.parse_table.get((top, current_token))
                if rule:
                    stack.extend(reversed(rule))
                else:
                    raise SyntaxError(f"Unexpected symbol '{current_token}' at position {pointer}")
            elif re.match(r'[a-zA-Z_][a-zA-Z0-9_]*', current_token) and top == 'id':
                pointer += 1
            elif re.match(r'[0-9]+', current_token) and top == '<constant>':
                pointer += 1
            else:
                raise SyntaxError(f"Invalid syntax at position {pointer} with symbol '{current_token}'")

        return pointer == len(tokens)

    def tokenize(self, input_string: str) -> List[str]:
        return re.findall(r'[a-zA-Z_][a-zA-Z0-9_]*|\d+|[{}();=+*/-]', input_string)

# Определение грамматики
grammar = Grammar(
    start_symbol='<statement>',
    non_terminals=['<statement>', '<assignment>', '<expression>', '<term>', '<factor>', '<function_call>', '<argument_list>', '<predicate>', '<variable>', '<constant>'],
    rules={
        '<statement>': [['<assignment>'], ['<function_call>'], ['<predicate>']],
        '<assignment>': [['<variable>', '=', '<expression>', ';']],
        '<expression>': [['<term>'], ['<term>', '+', '<expression>'], ['<term>', '-', '<expression>']],
        '<term>': [['<factor>'], ['<factor>', '*', '<term>'], ['<factor>', '/', '<term>']],
        '<factor>': [['<variable>'], ['<constant>'], ['(', '<expression>', ')']],
        '<function_call>': [['<variable>', '(', '<argument_list>', ')']],
        '<argument_list>': [['<expression>'], ['<expression>', ',', '<argument_list>']],
        '<predicate>': [['true'], ['false'], ['<expression>', '&&', '<expression>'], ['<expression>', '||', '<expression>']],
        '<variable>': [['id']],
        '<constant>': [['0'], ['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9']]
    }
)

# Таблица переходов
parse_table = {
    ('<statement>', 'id'): ['<assignment>'],
    ('<assignment>', 'id'): ['<variable>', '=', '<expression>', ';'],
    ('<expression>', 'id'): ['<term>'],
    ('<expression>', '0'): ['<term>'],
    ('<term>', 'id'): ['<factor>'],
    ('<factor>', 'id'): ['<variable>'],
    ('<factor>', '0'): ['<constant>'],
    ('<factor>', '('): ['(', '<expression>', ')'],
    ('<function_call>', 'id'): ['<variable>', '(', '<argument_list>', ')'],
    ('<argument_list>', 'id'): ['<expression>'],
    ('<predicate>', 'true'): ['true'],
    ('<predicate>', 'false'): ['false'],
    ('<predicate>', 'id'): ['<expression>']
}

# Клиентский код
if __name__ == "__main__":
    parser = LL1Parser(grammar, parse_table)
    input_string = "x = 3 + 2;"

    try:
        result = parser.parse(input_string)
        if result:
            print("Input string is valid")
        else:
            print("Input string is invalid")
    except SyntaxError as e:
        print(f"Error: {str(e)}")
