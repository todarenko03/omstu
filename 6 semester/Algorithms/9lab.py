import time
import random
import matplotlib.pyplot as plt

def all_variants(els: list) -> list:
    all_combinations = [[]]
    for item in els:
        current_comb = all_combinations[:]
        for comb in current_comb:
            all_combinations.append(comb + [item])
    
    return all_combinations

def complete_search(m: int, items: list[(int, int)]) -> tuple[list[int, int], int]:
    result = []
    maximum = 0
    for combo in all_variants(items):
        w = sum([x[0] for x in combo])
        s = sum([x[1] for x in combo])
        if w < m and s > maximum:
            result = combo
            maximum = s 
    
    return result, maximum

backpack = [(1, 10), (10, 1), (2, 5), (3, 6), (7, 12), (10, 10)]
weight = 12

print(complete_search(weight, backpack))

def greedy_alg(m: int, items: list[(int, int)]) -> tuple[list[int, int], int]:
    items.sort(reverse=True, key=lambda x: x[1] / x[0])
    result = []
    temp_weight = 0
    for item in items:
        if item[0] + temp_weight < m:
            result.append(item)
            temp_weight += item[0]
    return result, sum([x[1] for x in result])

backpack = [(1, 10), (10, 1), (2, 5), (3, 6), (7, 12), (10, 10)]
weight = 12

print(greedy_alg(weight, backpack))

times1 = []
times2 = []
x = [i for i in range(10, 30, 5)]
for i in x:
    print(i)
    start1 = time.time()
    complete_search(i // 2, [(j, random.randint(0, (i // 2) - 3), random.randint(j // 2, i * 2)) for j in range(10, i + i // 3)])
    finish1 = time.time()
    times1.append(finish1 - start1)
    start2 = time.time()
    greedy_alg(i // 2, [(j, random.randint(0, (i // 2) - 3), random.randint(j // 2, i * 2)) for j in range(10, i + i // 3)])
    finish2 = time.time()
    times2.append(finish2 - start2)

plt.plot(x, times1)
plt.plot(x, times2)
plt.xlabel = "size"
plt.ylabel = "time"
plt.legend(["complete_search", "greedy_alg"])
plt.show()