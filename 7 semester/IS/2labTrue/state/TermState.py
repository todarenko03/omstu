from node import BinOpNode
from state.State import State


class TermState(State):
    def handle(self, fsm, token):
        fsm.current_node = fsm.factor_state.handle(fsm, token)
        token = fsm.current_token
        while token.token_type in ('MULT', 'DIV'):
            op_token = token
            fsm.consume(op_token.token_type)
            fsm.current_node = BinOpNode(fsm.current_node, op_token.value, fsm.factor_state.handle(fsm, fsm.current_token))
            token = fsm.current_token
        return fsm.current_node
