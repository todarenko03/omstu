from building import Building

num_of_floors = 5
elevator1_position = 1
elevator2_position = 5

building = Building(
    num_of_floors=num_of_floors, 
    elevator1_position=elevator1_position, 
    elevator2_position=elevator2_position
)

actions = [(1, 2), (3, 1), (2, 4), (3, 5), (5, 4)]
