from task import Task
import numpy as np


class StateMachine:
    def __init__(self, task: Task, states: np.ndarray):
        self.task = task
        self.state = 0
        self.states = states

    def solve(self, n):
        for _ in range(n):
            is_food = self.task.smell_food()
            move, state = self.states[self.state, int(0 + 2 * is_food)], self.states[self.state, int(1 + 2 * is_food)]
            self.task.make_move_command(move)
            self.state = int(state)
        self.state = 0
        return self.task.road, self.task.collected_food
