import time
import random
import string
import numpy as np
import matplotlib.pyplot as plt


def list_to_dict(l):
    d = {}
    for i in l:
        d[i] = 0
        for j in l:
            if i == j:
                d[i] += 1
    return d


def huffman_coding(seq):
    result_tree = {}
    dict_seq = list_to_dict(seq)
    result_dict = dict.fromkeys(set(seq), '')
    
    for i in range(len(dict_seq) - 1):
        sorted_keys = sorted(dict_seq, key=dict_seq.get, reverse=True)
        new_dict = {}
        for key in sorted_keys:
            new_dict[key] = dict_seq[key]
        dict_seq = new_dict
        result_tree[0] = sorted_keys[-1]
        result_tree[1] = sorted_keys[-2]
        dict_seq[sorted_keys[-1] + sorted_keys[-2]] = dict_seq[sorted_keys[-1]] + dict_seq[sorted_keys[-2]]
        dict_seq.pop(sorted_keys[-1])
        dict_seq.pop(sorted_keys[-2])
        for symbol in sorted_keys[-1]:
           result_dict[symbol] += '0' 
        for symbol in sorted_keys[-2]:
           result_dict[symbol] += '1' 

    for key in result_dict:
        result_dict[key] = ''.join(reversed(result_dict[key]))

    return(result_dict)

tree = huffman_coding(['a', 'a', 'b', 'a', 'c', 'c', 'd', 'd'])

print(tree)

def generate_items(i):
    return [random.choice(string.ascii_lowercase) for _ in range(i)]


times1 = []
x = [i for i in range(10, 1000, 100)]
for i in x:
    print(i)
    start1 = time.time()
    huffman_coding(generate_items(i * 5))
    finish1 = time.time()
    times1.append(finish1 - start1)

plt.plot(x, times1)
plt.xlabel = "size"
plt.ylabel = "time"
plt.show()