import numpy as np


def cross_machines(parent0, parent1):
    parent0 = np.array(parent0)
    parent1 = np.array(parent1)
    kid0 = np.hstack((parent0[:, :2], parent1[: , 2:]))
    kid1 = np.hstack((parent1[:, :2], parent0[: , 2:]))
    return kid0, kid1
