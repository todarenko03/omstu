import numpy as np

class PCA():
    def __init__(self, n_components):
        self.n_components = n_components
        
    def fit_transform(self, X):
        X_scaled = (X - np.mean(X, axis=0)) / np.std(X, axis=0)
        cov_matrix = np.cov(X_scaled, rowvar=False)
        selfvalues, selfvectors = np.linalg.eig(cov_matrix)
        
        sorted_indices = np.argsort(selfvalues)[::-1]
        sorted_selfvalues = selfvalues[sorted_indices]
        sorted_selfvectors = selfvectors[sorted_indices]
        
        selfvectors_subset = sorted_selfvectors[:, :self.n_components]
        
        X_res = np.dot(X_scaled, selfvectors_subset)
        
        return X_res
        