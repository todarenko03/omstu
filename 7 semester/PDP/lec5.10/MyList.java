// public class MyList {
// 	private final List<Integer> items = Collections.synchronizedList(new ArrayList<>());

// 	public List<Integer> getItems() {
// 		return items;
// 	}

// 	public synchronized void putIfAbsent(Integer x) {
//         if (!items.contains(x)) {
//             items.add(x);
//         }
//     }
// }

public class MyList {
	private final List<Integer> items = new ArrayList<>();

    @Override 
    public synchronized boolean add(Integer x) {
        return items.add(x);
    }

	public synchronized void putIfAbsent(Integer x) {
        if (!items.contains(x)) {
            items.add(x);
        }
    }
}

