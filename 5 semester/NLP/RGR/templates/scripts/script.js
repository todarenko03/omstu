const container = document.getElementById('request-block')

function submitForm() {
    const toBeDeletedElements = document.querySelectorAll('.model-response');
    toBeDeletedElements.forEach(element => element.remove());
    const formData = new FormData(document.getElementById('form'));
    const textInput = formData.get('input');
    if (textInput.length != 0) {
        fetch("/predict", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ text: textInput}),
        })
        .then(response => response.json())
        .then(data => {
            const answer = document.createElement('div');
            answer.className = 'model-response';
            answer.textContent = data;
            container.appendChild(answer);
        })
        .catch(error => {
            console.error('Error', error);
        })
    }
}