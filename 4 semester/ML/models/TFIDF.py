import math
from collections import Counter

class TFIDF:
    def __init__(self):
        self.vocab = []
        self.idf = {}
        self.corpus = []
        self.tfidf_matrix = []

    def preprocess_corpus(self, documents):
        preprocessed_corpus = []
        for document in documents:
            terms = document.lower().split()
            preprocessed_terms = [term for term in terms if isinstance(term, str)]
            preprocessed_corpus.append(preprocessed_terms)
        return preprocessed_corpus

    def build_vocab(self):
        for document in self.corpus:
            for term in document:
                if term not in self.vocab:
                    self.vocab.append(term)

    def calculate_idf(self):
        word_documents = {term: 0 for term in self.vocab}
        total_documents = len(self.corpus)
        for document in self.corpus:
            unique_terms = set(document)
            for term in unique_terms:
                word_documents[term] += 1
        self.idf = {term: math.log((total_documents + 1) / (count + 1)) for term, count in word_documents.items()}

    def calculate_tf_idf(self):
        tf_idf_scores = []
        for document in self.corpus:
            term_counts = Counter(document)
            max_term_count = max(term_counts.values())
            tf = {term: count / max_term_count for term, count in term_counts.items()}
            tf_idf = {term: tf[term] * self.idf[term] if term in self.idf else 0 for term in tf}
            tf_idf_scores.append(tf_idf)
        return tf_idf_scores

    def fit_transform(self, documents, nan_value=0):
        self.corpus = self.preprocess_corpus(documents)
        self.build_vocab()
        self.calculate_idf()
        tf_idf_scores = self.calculate_tf_idf()

        for i in range(len(tf_idf_scores)):
            tfidf_vector = [tf_idf_scores[i][term] if term in tf_idf_scores[i] else nan_value for term in self.vocab]
            self.tfidf_matrix.append(tfidf_vector)

        return self.tfidf_matrix
