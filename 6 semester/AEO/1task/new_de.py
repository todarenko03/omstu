import random
import numpy as np

def generate_distance_matrix(n):
    matrix = [[0] * n for _ in range(n)]
    for i in range(n):
        matrix[i][i] = 1e+9
        for j in range(i + 1, n):
            matrix[i][j] = matrix[j][i] = random.randint(1, 100)
    return matrix

def total_distance(route, distance_matrix):
    total = 0
    for i in range(len(route) - 1):
        total += distance_matrix[int(route[i])][int(route[i + 1])]
    return total

def generate_individual(n):
    return random.sample(range(n), n)

def mutate(individual, indpb):
    size = len(individual)
    mutated = individual[:]
    for i in range(size):
        if random.random() < indpb:
            swap_index = random.randint(0, size - 1)
            mutated[i], mutated[swap_index] = mutated[swap_index], mutated[i]
    missing_cities = list(set(range(len(individual))) - set(mutated))
    mutated = list(set(mutated))
    random.shuffle(missing_cities)
    for city in missing_cities:
        if city not in mutated:
            mutated.append(city)
    return mutated

def select(population, k):
    return random.sample(population, k)

def initialize_population(population_size, n):
    return [generate_individual(n) for _ in range(population_size)]

def differential_evolution(distance_matrix, population_size=50, generations=100, cr=0.9, f=0.8, indpb=0.05):
    n = len(distance_matrix)
    population = initialize_population(population_size, n)

    for gen in range(generations):
        for i in range(population_size):
            a, b, c = select(population, 3)
            mutant = mutate([int(a_i + f * (b_i - c_i)) if (0 < int(a_i + f * (b_i - c_i)) < len(distance_matrix[0])) else a_i for a_i, b_i, c_i in zip(a, b, c)], indpb)
            crossed = [a_i if random.random() < cr else x_i for a_i, x_i in zip(a, mutant)]
            if len(list(set(crossed))) < len(distance_matrix):
                crossed = a
            crossed_distance = total_distance(crossed, distance_matrix)
            if crossed_distance < total_distance(population[i], distance_matrix):
                population[i] = crossed

    best_individual = min(population, key=lambda x: total_distance(x, distance_matrix))
    best_distance = total_distance(best_individual, distance_matrix)
    best_individual.append(best_individual[0])
    best_distance += distance_matrix[best_individual[-2]][best_individual[-1]]
    return best_individual, best_distance

distance_matrix = [[1e+9, 1, 2, 3, 4],
                   [5, 1e+9, 6, 7, 8],
                   [9, 10, 1e+9, 11, 12],
                   [13, 14, 15, 1e+9, 16],
                   [17, 18, 19, 20, 1e+9]]

best_route, best_distance = differential_evolution(distance_matrix)
# best_route, best_distance = differential_evolution(generate_distance_matrix(10))
print("Best Route:", best_route)
print("Best Distance:", best_distance)
