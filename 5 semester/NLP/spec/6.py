#Используя torchdata загрузить из текстового файла небольшой набор данных для машинного перевода. Выполнить токенизацию, сформировать вокабуляр с помощью torchtext.
import torchtext
import torchdata
from razdel import tokenize
from torchtext import transforms
from torchdata import datapipes
from torchtext.vocab import build_vocab_from_iterator

data_pipe = datapipes.iter.IterableWrapper(['data1.csv', 'data2.csv'])
data_pipe = datapipes.iter.FileOpener(data_pipe, mode='r', encoding='utf-8')

data_pipe = data_pipe.parse_csv(skip_lines=0, delimiter=',', as_tuple=True)

def tokenize(text: str) -> list[str]:
  return [token.text for token in tokenize(text)]

def yield_tokens(data_iter: datapipes.iter.IterDataPipe):
  for example, label in data_iter:
    yield tokenize(example)

vocab = build_vocab_from_iterator(
    yield_tokens(data_pipe),
    min_freq=1,
    specials= ['<pad>', '<sos>', '<eos>', '<unk>'],
    special_first=True
)

vocab.set_default_index(vocab['<unk>'])

def vocab_transform(vocab: torchtext.vocab.Vocab) -> transforms.Sequential:
    text_tranform = transforms.Sequential(
        transforms.VocabTransform(vocab=vocab),
        transforms.AddToken(vocab['<sos>'], begin=True),
        transforms.AddToken(vocab['<eos>'], begin=False)
    )
    return text_tranform

def apply_vocab_transform(pair: tuple[list[str], int]) -> tuple[list[int], int]:
    return (
        vocab_transform(vocab)(tokenize(pair[0])),
        int(pair[1])
    )

data_pipe = data_pipe.map(apply_vocab_transform)
