import numpy as np

class KMeans:
    def __init__(self, centers=2, iterations=500):
        self.centers = centers
        self.iterations = iterations
    
    
    def euclidean(self, x1, x2):
            
        return np.sqrt(np.sum((x1 - x2) ** 2, axis=1))
    
    
    def fit(self, data):
        
        self.centroids = [data[np.random.randint(0, len(data))]]
        
        for i in range(self.centers - 1):
            
            ind = np.random.randint(0, len(data))   
            self.centroids += [data[ind]]
            
        for i in range(self.iterations):
            
            sample = []
            
            for i in range(self.centers):
                sample.append([])
                
            for point in data:
                dist = self.euclidean(point, self.centroids)
                label = np.argmin(dist)
                sample[label].append(point)
                
            old_centroids = self.centroids
            
            for i in range(len(sample)):
                self.centroids[i] = np.mean(sample[i], axis=0)
            
            if np.equal(self.centroids, old_centroids).any():
                break
             
            
    def predict(self, data):
        
        res = np.array([])
        
        for point in data:
            dist = self.euclidean(point, self.centroids)
            res = np.append(res, np.argmin(dist))
            
        return res
