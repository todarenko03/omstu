package org.example.wordCounter;

import java.util.HashMap;
import java.util.Map;

public class WordCounter implements IWordCounter{
    private final Map<String, Integer> wordMap;

    public WordCounter() {
        wordMap = new HashMap<>();
    }

    public Map<String, Integer> calculate(final String text) {
        String[] wordsArray = text.split(" ");

        for (String word: wordsArray) {
            wordMap.put(word, wordMap.getOrDefault(word, 0) + 1);
        }

        return wordMap;
    }
}
