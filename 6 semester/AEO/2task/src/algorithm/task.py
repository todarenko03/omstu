from ant import Ant
from copy import deepcopy

class Task:
    def __init__(self, matrix):
        self.ant = Ant([len(matrix)-1, 0])
        self.matrix = deepcopy(matrix)
        self.road = []
        self.n = len(matrix)
        self.collected_food = 0

    def restart_task(self, matrix):
        self.__init__(matrix)

    def make_move_ant(self):
        self.ant.move()
        is_food = self.matrix[self.ant.coords[0]][self.ant.coords[1]]
        self.road.append((self.ant.coords[0], self.ant.coords[1], is_food))
        self.collected_food += is_food
        self.matrix[self.ant.coords[0]][self.ant.coords[1]] = 0

    def make_move_command(self, move):
        if move == 0:
            if self.ant.direction == 1 and self.ant.coords[1] + 1 < self.n:
                self.make_move_ant()
            elif self.ant.direction == 0 and self.ant.coords[0] + 1 < self.n:
                self.make_move_ant()
            elif self.ant.direction == 2 and self.ant.coords[0] - 1 >= 0:
                self.make_move_ant()
            elif self.ant.direction == 3 and self.ant.coords[1] - 1 >= 0:
                self.make_move_ant()
        else:
            self.ant.rotate(move)

    def smell_food(self):
        if self.ant.direction == 1 and self.ant.coords[1] + 1 < self.n:
            return self.matrix[self.ant.coords[0]][self.ant.coords[1] + 1]
        if self.ant.direction == 0 and self.ant.coords[0] + 1 < self.n:
            return self.matrix[self.ant.coords[0] + 1][self.ant.coords[1]]
        if self.ant.direction == 2 and self.ant.coords[0] - 1 > 0:
            return self.matrix[self.ant.coords[0] - 1][self.ant.coords[1]]
        if self.ant.direction == 3 and self.ant.coords[1] - 1 > 0:
            return self.matrix[self.ant.coords[0]][self.ant.coords[1] - 1]
        return 0


