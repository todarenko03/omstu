import time
import matplotlib.pyplot as plt

# Провести анализ временной зависимости вычисления элемента 
# последовательности элемента Фибоначчи, для итерационного и рекурсивного алгоритма.

def recursive_fibonacci(n):
    if n <= 1:
        return 1
    return recursive_fibonacci(n - 1) + recursive_fibonacci(n - 2)

def iteration_fibonacci(n):
    if n <= 1:
        return n
    first = 0
    second = 1
    for i in range(2, n + 1):
        first, second = second, first + second
    return second

recursive_fibonacci_times = []
iteration_fibonacci_times = []
for el in range(1, 45, 5):
    start1 = time.time()
    recursive_fibonacci(el)
    finish1 = time.time()
    start2 = time.time()
    iteration_fibonacci(el)
    finish2 = time.time()
    recursive_fibonacci_times.append(finish1 - start1)
    iteration_fibonacci_times.append(finish2 - start2)
    print(finish1 - start1, ' - ', finish2 - start2)

n = [i for i in range(1, 45, 5)]
plt.plot(n, recursive_fibonacci_times)
plt.plot(n, iteration_fibonacci_times)
plt.xlabel('Число')
plt.ylabel('Время выполнения')
plt.legend(['Рекурсия', 'Итерации'])
plt.show()
