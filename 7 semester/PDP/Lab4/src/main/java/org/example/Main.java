package org.example;

import org.example.foundingBot.ISearchBot;
import org.example.foundingBot.SearchBot;

import java.io.File;
import java.util.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Map<String, List<String>> hierarchy = new HashMap<>();

        File directory = new File("/home/todarenko03/Downloads/spring-boot-main/spring-boot-project/spring-boot/src/");

        ISearchBot searchBot = new SearchBot();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                searchBot.search(directory, hierarchy);
            }
        });
        thread.start();
        thread.join();
        hierarchy.forEach((key, value) -> System.out.println(key + ": " + value.toString()));
    }
}
