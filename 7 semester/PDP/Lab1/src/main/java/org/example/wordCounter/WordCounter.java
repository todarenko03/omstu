package org.example.wordCounter;

import java.util.HashMap;
import java.util.Map;

public class WordCounter {
    private final Map<String, Integer> wordMap;

    public WordCounter() {
        wordMap = new HashMap<>();
    }

    public Map<String, Integer> calculate(String text) {
        String[] wordsArray = text.split(" ");

        for (String word: wordsArray) {
            wordMap.put(word, wordMap.getOrDefault(word, 0) + 1);
        }

        return wordMap;
    }
}
