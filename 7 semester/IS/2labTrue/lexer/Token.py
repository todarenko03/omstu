class Token:
    def __init__(self, token_type: str, value: str):
        self.token_type = token_type
        self.value = value

    def __repr__(self):
        return f"Token({self.token_type}, {self.value})"
