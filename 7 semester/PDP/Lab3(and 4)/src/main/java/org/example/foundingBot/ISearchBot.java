package org.example.foundingBot;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface ISearchBot {
    void search(File folder, Map<String, List<String>> hierarchy);
}
