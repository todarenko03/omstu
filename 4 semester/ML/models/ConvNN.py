import numpy as np
from sklearn.metrics import mean_squared_error
from scipy.special import expit 
from scipy.signal import convolve2d
from copy import deepcopy

def linear(inputs):

    return inputs

def linear_derivative(inputs):

    return 1

def relu(inputs):

    return np.maximum(0, inputs)

def relu_derivative(inputs):
    return (inputs >= 0).astype(float)

def sigmoid(inputs):
        
    return expit(inputs)

def sigmoid_derivative(inputs):

    return sigmoid(inputs) * (1 - sigmoid(inputs))

def tanh(inputs):

    return (2 / (1 + np.exp(-2 * inputs))) 

def tanh_derivative(inputs):
    
    return 1 - tanh(inputs) ** 2

class Conv2D:
    def __init__(self, kernel_num, kernel_size, activation='linear', padding='valid', num_filters=3):
        self.kernel_num = kernel_num
        self.kernel_size = kernel_size
        self.neurons = kernel_size * kernel_num
        self.activation = activation
        self.padding = padding
        self.kernel_bias = None
        self.size = None
        self.kernels = np.random.uniform(-1, 1, size=(num_filters, kernel_num, kernel_size, kernel_size))
        self.derivatives = {'linear': linear_derivative,
                            'relu': relu_derivative,
                            'sigmoid': sigmoid_derivative,
                            'tanh': tanh_derivative}
      
    def forward(self, inputs, weights, bias, activation, size):
        A_curr = np.array([])  
        self.size = size
        print('inputs.shape: ', inputs.shape)
        for i in range(self.kernel_num):
            res = 0
            for j in range(inputs.shape[3]):
                res += convolve2d(inputs[0,:,:,j], self.kernels[j][i])
            res += self.kernel_bias[i]
            A_curr = np.append(A_curr, res)
        
        return A_curr.reshape((1, size[0] + self.kernel_size - 1, size[1] + self.kernel_size - 1, self.kernel_num))


class Dence:
    def __init__(self, neurons, activation):

        self.neurons = neurons
        self.activation = activation
        self.derivatives = {'linear': linear_derivative,
                            'relu': relu_derivative,
                            'sigmoid': sigmoid_derivative,
                            'tanh': tanh_derivative}
        self.activations = {'linear': linear,
                            'relu': relu,
                            'sigmoid': sigmoid,
                            'tanh': tanh}

    def forward(self, inputs, weights, bias, activation, size):
        
        Z_curr = np.dot(inputs, weights.T) + bias
        
        return self.activations[activation](Z_curr)


class Network:
    def __init__(self, e=0.01, size=[200, 200]):

        self.network = np.array([])
        self.architecture = np.array([])
        self.weights = np.array([])
        self.memory = np.array([])
        self.gradient = []
        self.size = size
        self.init_size = deepcopy(size)
        self.e = e

    def add(self, layer):

        self.network = np.append(self.network, layer)

    def _compile(self, data):

        for idx, layer in enumerate(self.network):

            if idx == 0:
                if isinstance(self.network[idx], Conv2D):
                    self.architecture = np.append(self.architecture, {'input_dim': 666,
                                                                      'output_dim': 666,
                                                                      'activation': 666})
                    self.size[0] += self.network[idx].kernel_size - 1
                    self.size[1] += self.network[idx].kernel_size - 1
                    print('size: ', self.size)
                    self.network[idx].kernel_bias = np.zeros((self.network[0].kernel_num, self.size[0], self.size[1]))
                    
                else:
                    self.architecture = np.append(self.architecture, {'input_dim': len(list(data)),
                                                                      'output_dim': self.network[idx].neurons,
                                                                      'activation': self.network[idx].activation})
            else:
                if isinstance(self.network[idx], Conv2D):
                    self.architecture = np.append(self.architecture, {'input_dim': 666,
                                                                      'output_dim': 666,
                                                                      'activation': 666})
                    self.size[0] += self.network[idx].kernel_size - 1
                    self.size[1] += self.network[idx].kernel_size - 1
                    print('size: ', self.size)
                    self.network[idx].kernel_bias = np.zeros((self.network[idx].kernel_num, self.size[0], self.size[1]))
                else:
                    if isinstance(self.network[idx - 1], Conv2D):
                        self.architecture = np.append(self.architecture, {'input_dim': ((self.size[0]) * (self.size[1]) * self.network[idx - 1].kernel_num),
                                                                      'output_dim': self.network[idx].neurons,
                                                                      'activation': self.network[idx].activation})
                        print('input_dim: ', self.architecture[idx]['input_dim'])
                    else:    
                        self.architecture = np.append(self.architecture, {'input_dim': self.network[idx - 1].neurons,
                                                                      'output_dim': self.network[idx].neurons,
                                                                      'activation': self.network[idx].activation})

        self.size = self.init_size
        
    def init_weights(self, data):

        self._compile(data)
        np.random.seed(42)

        for i in range(len(self.architecture)):
            if isinstance(self.network[i], Conv2D):
#                 self.network[i].kernel_bias = np.random.uniform(-1, 1, size=(((self.size[0] + self.network[i].kernel_size - 1), (self.size[1] + self.network[i].kernel_size - 1))))
#                 print('[+]', self.size[0], self.network[i].kernel_size)
#                 print(self.network[i].kernel_bias.shape)
                self.weights = np.append(self.weights, {'W': np.zeros((666, 666)), 'b': np.zeros((666, 666))}) 
                
            else:
                self.weights = np.append(self.weights, {'W': np.random.uniform(-1, 1, size=
                    (self.architecture[i]['output_dim'], self.architecture[i]['input_dim'])),
                                                    'b': np.zeros((1, self.architecture[i]['output_dim']))})
    def forwardprop(self, data):
#         print(self.weights)
        A_curr = data

        for i in range(len(self.weights)):
            print('iter in forw: ', i)
            if i > 0 and isinstance(self.network[i - 1], Conv2D) and not isinstance(self.network[i], Conv2D):
                A_curr = A_curr.flatten()
            A_prev = A_curr
            A_curr = self.network[i].forward(A_prev,
                                                     self.weights[i]['W'],
                                                     self.weights[i]['b'],
                                                     self.architecture[i]['activation'],
                                                     self.size)       
            if isinstance(self.network[i], Conv2D):
                        self.size[0] += self.network[i].kernel_size - 1
                        self.size[1] += self.network[i].kernel_size - 1
                        
            self.memory = np.append(self.memory, {'inputs': A_prev})
        
        self.size = self.init_size
        self.size[0] -= 1
        self.size[1] -= 1
        
        return A_curr

    def backprop(self, predicted, actual):
        
        self.gradient = np.array([])
        errors = predicted - actual
        
        for i in reversed(range(len(self.network))):
            
            print('back: ', i)
            
            if isinstance(self.network[i], Conv2D):
                print('input shape back', self.memory[i]['inputs'].shape[3])
                cgradsW = np.array([])
                cgradsb = np.array([])
                kerSize = 0
                for k in range(self.network[i].kernel_num):
                    gradW = np.array([])
                    gradb = np.array([])
                    for j in range(self.memory[i]['inputs'].shape[3]):
                        gradW = np.append(gradW, convolve2d(errors, self.memory[i]['inputs'][0,:,:,j]))
                        kerSize = gradW.shape
                        gradb = np.append(gradb, errors)
                    cgradsW = np.append(cgradsW, gradW)
                    cgradsb = np.append(cgradsb, gradb)
                self.gradient = np.append(self.gradient, {'W': cgradsW.reshape(self.memory[i]['inputs'].shape[3], self.network[i].kernel_num, -1),'b': cgradsb.reshape(self.network[i].kernel_num, -1)})
                res = 0
                for j in range(self.network[i].kernels.shape[0]):
                    for k in range(self.network[i].kernels.shape[1]):
                        res += convolve2d(errors, self.network[i].kernels[j][k])
                errors = res
                
                print('gradW for conv: ', cgradsW)
                
            else:
                if isinstance(self.network[i - 1], Conv2D):
                    self.memory[i]['inputs'] = self.memory[i]['inputs'].reshape((1, -1))
                grad1 = np.dot(self.memory[i]['inputs'].T, errors)
                grad2 = errors
                self.gradient = np.append(self.gradient, {'W': grad1,'b': grad2})
                errors = np.dot(errors, self.weights[i]['W']) * self.network[i].derivatives[self.architecture[i]['activation']](self.memory[i]['inputs'])
        self.gradient = np.flip(self.gradient)
        
    def update_weights(self, data):        
        for i in range(len(self.weights)):
            if isinstance(self.network[i], Conv2D):
                for j in range(self.network[i].kernels.shape[1]):
                    for k in range(self.network[i].kernels.shape[0]):
                        print('kernelsSize: ', self.network[i].kernels.shape)
                        print('gradientsSize: ', self.gradient[i]['W'].shape)
                        print('biasSize', self.gradient[i]['b'].shape)
                        self.network[i].kernels[k][j] -= self.e * self.gradient[i]['W'][k][j][:self.network[i].kernels[k][j].shape[0]**2].reshape((self.network[i].kernels[k][j].shape))
                    self.network[i].kernel_bias -= self.e * self.gradient[i]['b'][j][:self.network[i].kernel_bias[j].shape[0]**2].reshape((self.network[i].kernel_bias[j].shape))
            else:
                self.weights[i]['W'] -= (self.e * self.gradient[i]['W']).T
                self.weights[i]['b'] -=  self.e * self.gradient[i]['b']
            
    def train(self, X_train, epochs):
        y_train = np.asarray(list(X_train.map(lambda x, y: y)))
        X_train = np.asarray(list(X_train.map(lambda x, y: x / 255)))
        self.init_weights(X_train)
        for i in range(epochs):
            y_pred_res = np.array([])
            for i in range(X_train.shape[1]):
                print('[+]', X_train.shape)
                y_pred = self.forwardprop(np.asarray(X_train[i]))
                y_pred_res = np.append(y_pred_res, y_pred)
                self.backprop(y_pred, y_train[i])
                self.update_weights(X_train[i])
            print('[+]MSE: ', mean_squared_error(y_pred_res, y_train))
    def predict(self, X_test):
        return self.forwardprop(X_test)