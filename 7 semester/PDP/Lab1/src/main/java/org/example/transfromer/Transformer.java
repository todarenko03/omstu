package org.example.transfromer;

public class Transformer {

    public Transformer() {}

    public String transform(final String text, final String template, final String replacing) {
        return text.replaceAll(template, replacing);
    }
}
