from tkinter import *

from docx import Document
import openpyxl
import os.path


class Expert:
    def f1(self):
        self.txt.grid_remove()
        txt = "С вами связались для предложение «суперпредложения»?"
        self.txt1 = Label(self.window, text=txt, width=80, height=15, bg="cornflower blue", font=("Century gothic", 14))
        self.txt1.grid()
        self.ButYes = Button(self.window, bg="orange", text="Да", font=("Century gothic", 20), command=lambda: self.f12())
        self.ButYes.place(relx=0.05, rely=0.8)
        self.ButYes1 = Button(self.window, bg="orange", text="Скорее да", font=("Century gothic", 20), command=lambda: self.f12())
        self.ButYes1.place(relx=0.175, rely=0.8)
        self.ButNo1 = Button(self.window, bg="orange", text="Не знаю", font=("Century gothic", 20), command=lambda: self.f11())
        self.ButNo1.place(relx=0.425, rely=0.8)
        self.ButNo2 = Button(self.window, bg="orange", text="Скорее нет", font=("Century gothic", 20), command=lambda: self.f11())
        self.ButNo2.place(relx=0.625, rely=0.8)
        self.ButNo = Button(self.window, bg="orange", text="Нет", font=("Century gothic", 20), command=lambda: self.f11())
        self.ButNo.place(relx=0.875, rely=0.8)

    def f2(self):
        self.txt.grid_remove()
        txt = "Письмо/сообщение/звонок оказывает на вас давление?"
        self.txt1 = Label(self.window, text=txt, width=80, height=15, bg="cornflower blue", font=("Century gothic", 14))
        self.txt1.grid()
        self.ButYes = Button(self.window, bg="orange", text="Да", font=("Century gothic", 20), command=lambda: self.f22())
        self.ButYes.place(relx=0.05, rely=0.8)
        self.ButYes1 = Button(self.window, bg="orange", text="Скорее да", font=("Century gothic", 20), command=lambda: self.f22())
        self.ButYes1.place(relx=0.175, rely=0.8)
        self.ButNo1 = Button(self.window, bg="orange", text="Не знаю", font=("Century gothic", 20), command=lambda: self.f21())
        self.ButNo1.place(relx=0.425, rely=0.8)
        self.ButNo2 = Button(self.window, bg="orange", text="Скорее нет", font=("Century gothic", 20), command=lambda: self.f21())
        self.ButNo2.place(relx=0.625, rely=0.8)
        self.ButNo = Button(self.window, bg="orange", text="Нет", font=("Century gothic", 20), command=lambda: self.f21())
        self.ButNo.place(relx=0.875, rely=0.8)

    def f12(self):
        self.window.destroy()
        self.window2 = Tk()
        self.window2.configure(bg="cornflower blue")
        self.window2.geometry('900x500')
        self.window2.title("Expert system")
        txt = "Результат: Скорее всего вы подверглись фишинговой атаке."
        txt2 = "Рекомендация: Проверьте в интернете, существует ли возможность"
        txt3 = "такого предложения и его легальность."
        self.txt = Label(self.window2, text=txt, width=80, height=2, bg="cornflower blue", font=("Century gothic", 14))
        self.txt.grid()
        self.txt2 = Label(self.window2, text=txt2, width=80, height=0, bg="cornflower blue", font=("Century gothic", 14))
        self.txt2.grid()
        self.txt3 = Label(self.window2, text=txt3, width=80, height=0, bg="cornflower blue",
                          font=("Century gothic", 14))
        self.txt3.grid()
        file.add_paragraph(txt + "\n" + "\n" + txt2 + "\n" + "\n")
        file.save('expert.docx')
        exel_list['A2'] = "1"
        exel_list['B1'] = "Вопрос"
        exel_list['C1'] = "Значение"
        exel_list['B2'] = "Вы получили письмо/сообщение/звонок с просьбой подтвердить" \
                          " личную информацию?"
        exel_list['C2'] = "False"
        exel_list['A3'] = "2"
        exel_list['B3'] = "С вами связались для предложение «суперпредложения» "
        exel_list['C3'] = "True"
        exel_list['A4'] = "3"
        exel_list['B4'] = "Результат"
        exel_list['C4'] = "Скорее всего вы подверглись фишинговой атаке"
        excel.save('Protocol.xlsx')

    def f11(self):
        self.txt1.grid_remove()
        txt = "Вы получили письмо, содержащее подозрительные ссылки?"
        self.txt2 = Label(self.window, text=txt, width=80, height=15, bg="cornflower blue", font=("Century gothic", 14))
        self.txt2.grid()
        self.ButYes = Button(self.window, bg="orange", text="Да", font=("Century gothic", 20), command=lambda: self.f112())
        self.ButYes.place(relx=0.05, rely=0.8)
        self.ButYes1 = Button(self.window, bg="orange", text="Скорее да", font=("Century gothic", 20), command=lambda: self.f112())
        self.ButYes1.place(relx=0.175, rely=0.8)
        self.ButNo1 = Button(self.window, bg="orange", text="Не знаю", font=("Century gothic", 20), command=lambda: self.f111())
        self.ButNo1.place(relx=0.425, rely=0.8)
        self.ButNo2 = Button(self.window, bg="orange", text="Скорее нет", font=("Century gothic", 20), command=lambda: self.f111())
        self.ButNo2.place(relx=0.625, rely=0.8)
        self.ButNo = Button(self.window, bg="orange", text="Нет", font=("Century gothic", 20), command=lambda: self.f111())
        self.ButNo.place(relx=0.875, rely=0.8)

    def f111(self):
        self.window.destroy()
        self.window2 = Tk()
        self.window2.configure(bg="cornflower blue")
        self.window2.geometry('900x500')
        self.window2.title("Expert system")
        txt = "Результат: Скорее всего вам не угрожает фишинговая атака."
        self.txt = Label(self.window2, text=txt, width=80, height=2, bg="cornflower blue", font=("Century gothic", 14))
        self.txt.grid()
        file.add_paragraph(txt + "\n")
        file.save('expert.docx')
        file.add_paragraph(txt + "\n" + "\n" + txt2 + "\n" + "\n")
        file.save('expert.docx')
        exel_list['A2'] = "1"
        exel_list['B1'] = "Вопрос"
        exel_list['C1'] = "Значение"
        exel_list['B2'] = "Вы получили письмо/сообщение/звонок с просьбой подтвердить" \
                          " личную информацию?"
        exel_list['C2'] = "False"
        exel_list['A3'] = "2"
        exel_list['B3'] = "С вами связались для предложение «суперпредложения» "
        exel_list['C3'] = "False"
        exel_list['A4'] = "3"
        exel_list['B4'] = "Вы получили письмо, содержащее подозрительные ссылки?"
        exel_list['C4'] = "False"
        exel_list['A5'] = "4"
        exel_list['B5'] = "Результат"
        exel_list['C5'] = "Скорее всего вам не угрожает фишинговая атака"
        excel.save('Protocol.xlsx')

    def f112(self):
        self.window.destroy()
        self.window2 = Tk()
        self.window2.configure(bg="cornflower blue")
        self.window2.geometry('900x500')
        self.window2.title("Expert system")
        txt = "Результат: Скорее всего вы подверглись фишинговой атаке. "
        txt2 = "Рекомендация:  Не переходите по ссылкам, которым вы не доверяете."
        self.txt = Label(self.window2, text=txt, width=80, height=2, bg="cornflower blue", font=("Century gothic", 14))
        self.txt.grid()
        self.txt2 = Label(self.window2, text=txt2, width=80, height=0, bg="cornflower blue",
                          font=("Century gothic", 14))
        self.txt2.grid()
        file.add_paragraph(txt + "\n" + "\n" + txt2 + "\n" + "\n")
        file.save('expert.docx')
        exel_list['A2'] = "1"
        exel_list['B1'] = "Вопрос"
        exel_list['C1'] = "Значение"
        exel_list['B2'] = "Вы получили письмо/сообщение/звонок с просьбой подтвердить" \
                          " личную информацию?"
        exel_list['C2'] = "False"
        exel_list['A3'] = "2"
        exel_list['B3'] = "С вами связались для предложение «суперпредложения» "
        exel_list['C3'] = "False"
        exel_list['A4'] = "3"
        exel_list['B4'] = "Вы получили письмо, содержащее подозрительные ссылки?"
        exel_list['C4'] = "True"
        exel_list['A5'] = "4"
        exel_list['B5'] = "Результат"
        exel_list['C5'] = "Скорее всего вы подверглись фишинговой атаке"
        excel.save('Protocol.xlsx')

    def f21(self):
        self.txt1.grid_remove()
        txt = "Содержит ли письмо/сообщение/речь ошибки, странные фразеологическое обороты"
        self.txt2 = Label(self.window, text=txt, width=80, height=15, bg="cornflower blue", font=("Century gothic", 14))
        self.txt2.grid()
        self.ButYes = Button(self.window, bg="orange", text="Да", font=("Century gothic", 20), command=lambda: self.f212())
        self.ButYes.place(relx=0.05, rely=0.8)
        self.ButYes1 = Button(self.window, bg="orange", text="Скорее да", font=("Century gothic", 20), command=lambda: self.f212())
        self.ButYes1.place(relx=0.175, rely=0.8)
        self.ButNo1 = Button(self.window, bg="orange", text="Не знаю", font=("Century gothic", 20), command=lambda: self.f211())
        self.ButNo1.place(relx=0.425, rely=0.8)
        self.ButNo2 = Button(self.window, bg="orange", text="Скорее нет", font=("Century gothic", 20), command=lambda: self.f211())
        self.ButNo2.place(relx=0.625, rely=0.8)
        self.ButNo = Button(self.window, bg="orange", text="Нет", font=("Century gothic", 20), command=lambda: self.f211())
        self.ButNo.place(relx=0.875, rely=0.8)

    def f211(self):
        self.window.destroy()
        self.window2 = Tk()
        self.window2.configure(bg="cornflower blue")
        self.window2.geometry('900x500')
        self.window2.title("Expert system")
        txt = "Результат: Есть вероятность того, что вы подверглись фишинговой атаке. "
        txt2 = "Рекомендация: Будьте бдительны, не совершайте поспешных действий,"
        txt3 = " внимательно рассмотрите ситуацию."
        self.txt = Label(self.window2, text=txt, width=80, height=2, bg="cornflower blue", font=("Century gothic", 14))
        self.txt.grid()
        self.txt2 = Label(self.window2, text=txt2, width=80, height=0, bg="cornflower blue", font=("Century gothic", 14))
        self.txt2.grid()
        self.txt3 = Label(self.window2, text=txt3, width=80, height=0, bg="cornflower blue",
                          font=("Century gothic", 14))
        self.txt3.grid()
        file.add_paragraph(txt + "\n" + "\n" + txt2 + "\n" + "\n" + txt3 + "\n" + "\n")
        file.save('expert.docx')
        exel_list['A2'] = "1"
        exel_list['B1'] = "Вопрос"
        exel_list['C2'] = "Значение"
        exel_list['B2'] = "Вы получили письмо/сообщение/звонок с просьбой подтвердить" \
                          " личную информацию?"
        exel_list['C2'] = "True"
        exel_list['A3'] = "2"
        exel_list['B3'] = "Письмо/сообщение/звонок оказывает на вас давление?"
        exel_list['C3'] = "False"
        exel_list['A4'] = "3"
        exel_list['B4'] = "Содержит ли письмо/сообщение/речь ошибки, странные " \
                          "фразеологическое обороты"
        exel_list['C4'] = "False"
        exel_list['A5'] = "4"
        exel_list['B5'] = "Результат"
        exel_list['C5'] = "Есть вероятность того, что вы подверглись фишинговой атаке"
        excel.save('Protocol.xlsx')

    def f212(self):
        self.window.destroy()
        self.window2 = Tk()
        self.window2.configure(bg="cornflower blue")
        self.window2.geometry('900x500')
        self.window2.title("Expert system")
        txt = "Результат: Скорее всего вы подверглись фишинговой атаке.  "
        txt2 = "Рекомендация: Не оставляйте свои данные, если письмо/сообщение/звонок"
        txt3 = "не внушают вам доверия."
        self.txt = Label(self.window2, text=txt, width=80, height=2, bg="cornflower blue", font=("Century gothic", 14))
        self.txt.grid()
        self.txt2 = Label(self.window2, text=txt2, width=80, height=0, bg="cornflower blue", font=("Century gothic", 14))
        self.txt2.grid()
        self.txt3 = Label(self.window2, text=txt3, width=80, height=0, bg="cornflower blue",
                          font=("Century gothic", 14))
        self.txt3.grid()
        file.add_paragraph(txt + "\n" + "\n" + txt2 + "\n" + "\n" + txt3 + "\n" + "\n")
        file.save('expert.docx')
        exel_list['A2'] = "1"
        exel_list['B1'] = "Вопрос"
        exel_list['C1'] = "Значение"
        exel_list['B2'] = "Вы получили письмо/сообщение/звонок с просьбой подтвердить" \
                          " личную информацию?"
        exel_list['C2'] = "True"
        exel_list['A3'] = "2"
        exel_list['B3'] = "Письмо/сообщение/звонок оказывает на вас давление?"
        exel_list['C3'] = "False"
        exel_list['A4'] = "3"
        exel_list['B4'] = "Содержит ли письмо/сообщение/речь ошибки, странные " \
                          "фразеологическое обороты"
        exel_list['C4'] = "True"
        exel_list['A5'] = "4"
        exel_list['B5'] = "Результат"
        exel_list['C5'] = "Скорее всего вы подверглись фишинговой атаке"
        excel.save('Protocol.xlsx')

    def f22(self):
        self.window.destroy()
        self.window2 = Tk()
        self.window2.configure(bg="cornflower blue")
        self.window2.geometry('900x500')
        self.window2.title("Expert system")
        txt = "Результат: Скорее всего вы подверглись фишинговой атаке."
        txt2 = "Рекомендация: Если вам звонят от имени какой-либо кампании,"
        txt3 = "позвоните им лично и уточните информацию."
        self.txt = Label(self.window2, text=txt, width=80, height=2, bg="cornflower blue", font=("Century gothic", 14))
        self.txt.grid()
        self.txt2 = Label(self.window2, text=txt2, width=80, height=0, bg="cornflower blue", font=("Century gothic", 14))
        self.txt2.grid()
        self.txt3 = Label(self.window2, text=txt3, width=80, height=0, bg="cornflower blue",
                          font=("Century gothic", 14))
        self.txt3.grid()
        file.add_paragraph(txt + "\n" + "\n" + txt2 + "\n" + "\n" + txt3 + "\n" + "\n")
        file.save('expert.docx')

        exel_list['A2'] = '1'
        exel_list['B1'] = 'Вопрос'
        exel_list['C1'] = 'Значение'
        exel_list['B2'] = 'Вы получили письмо/сообщение/звонок с просьбой подтвердить' \
                          ' личную информацию?'
        exel_list['C2'] = 'True'
        exel_list['A3'] = '2'
        exel_list['B3'] = 'Письмо/сообщение/звонок оказывает на вас давление?'
        exel_list['C3'] = 'True'
        exel_list['A4'] = '3'
        exel_list['B4'] = 'Результат'
        exel_list['C4'] = 'Скорее всего вы подверглись фишинговой атаке'
        excel.save('Protocol.xlsx')

    def f(self):
        self.window = Tk()
        self.window.geometry('900x500')
        self.window.title("Expert system")
        self.window.configure(bg="cornflower blue")
        txt = "Вас приветствует виртуальный экспетр по " \
              "обнаружению фишинговых атак"
        lbl = Label(self.window, text=txt, bg="linen", font=("Century gothic bold", 19))
        lbl.grid()
        txt = "Вы получили письмо/сообщение/звонок с " \
              "просьбой подтвердить личную информацию?"
        self.txt = Label(self.window, text=txt, width=80, height=15, bg="cornflower blue", font=("Century gothic", 14))
        self.txt.grid()
        self.ButYes = Button(self.window, bg="orange", text="Да", font=("Century gothic", 20), command=lambda: self.f2())
        self.ButYes.place(relx=0.05, rely=0.8)
        self.ButYes1 = Button(self.window, bg="orange", text="Скорее да", font=("Century gothic", 20), command=lambda: self.f2())
        self.ButYes1.place(relx=0.175, rely=0.8)
        self.ButNo1 = Button(self.window, bg="orange", text="Не знаю", font=("Century gothic", 20), command=lambda: self.f1())
        self.ButNo1.place(relx=0.425, rely=0.8)
        self.ButNo2 = Button(self.window, bg="orange", text="Скорее нет", font=("Century gothic", 20), command=lambda: self.f1())
        self.ButNo2.place(relx=0.625, rely=0.8)
        self.ButNo = Button(self.window, bg="orange", text="Нет", font=("Century gothic", 20), command=lambda: self.f1())
        self.ButNo.place(relx=0.875, rely=0.8)
        self.window.mainloop()


def c():
    file.add_paragraph(familia.get() + " " + name.get() + " " + surname.get() + " ")
    window0.destroy()
    a = Expert()
    a.f()


if os.path.exists('expert.docx'):
    file = Document('expert.docx')
else:
    file = Document()
if not os.path.exists('Protocol.xlsx'):
    nexcel = openpyxl.Workbook()
    nexcel.save(filename="Protocol.xlsx")
excel = openpyxl.load_workbook(r'C:\Users\HP\PycharmProjects\ExpertSystem\Protocol.xlsx')
exel_list = excel['Sheet']
window0 = Tk()
window0.geometry('900x500')
window0.title("Expert system")
window0.configure(bg="cornflower blue")
txt = "Вас приветствует виртуальный экспетр по " \
      "обнаружению фишинговых атак"
txt2 = "Введите свои данные"
lbl = Label(window0, text=txt, bg="linen", font=("Century gothic bold", 19))
lbl.grid(row=1)
lbl = Label(window0, text=txt2, bg="linen", font=("Century gothic bold", 19))
lbl.grid(row=2)
txtF = "Фамилия: "
txtN = "Имя: "
txtS = "Отчество: "
lbl1 = Label(window0, text=txtF, bg="linen", font=("Century gothic bold", 10))
lbl1.place(relx=0.01, rely=0.3)
lbl2 = Label(window0, text=txtN, bg="linen", font=("Century gothic bold", 10))
lbl2.place(relx=0.01, rely=0.375)
lbl3 = Label(window0, text=txtS, bg="linen", font=("Century gothic bold", 10))
lbl3.place(relx=0.01, rely=0.45)
familia = Entry(window0, width=30)
familia.place(relx=0.125, rely=0.3)
name = Entry(window0, width=30)
name.place(relx=0.125, rely=0.375)
surname = Entry(window0, width=30)
surname.place(relx=0.125, rely=0.45)
ButYes = Button(window0, bg="orange", text="Ввести", font=("Century gothic", 20), command=lambda: c())
ButYes.place(relx=0.05, rely=0.8)
window0.mainloop()
