import time
import random
import string
import matplotlib.pyplot as plt

def naive(s: str, template: str) -> int:
    count = 0
    for i in range(len(s) - len(template) + 1):
        if s[i:len(template) + i] == template:
            count += 1
    return count

print(naive("strrstrrstra", "str"))

def bad_character_table(pattern):
    table = {}
    for i in range(len(pattern)-1):
        table[pattern[i]] = i
    return table

def good_suffix_table(pattern):
    s = pattern + pattern
    table = [0] * (len(pattern) + 1)
    for i in range(len(pattern) - 1, -1, -1):
        step = 1
        for j in range(i):
            if pattern[len(pattern) - 1 - i: len(pattern) - 1] == pattern[len(pattern) - 1 - i - j: len(pattern) - 1 - j]:
                table[i] = step
                break
            step += 1
    table[0] = table[1]
    return table

def boyer_moore(text, pattern):
    count = 0
    bad_char_table = bad_character_table(pattern)
    suffix_table = good_suffix_table(pattern)
    i = 0
    while i <= len(text) - len(pattern):
        j = len(pattern) - 1
        while j >= 0 and pattern[j] == text[i + j]:
            j -= 1
        if j < 0:
            count += 1
            i += suffix_table[0]
        else:
            bad_char_offset = j - bad_char_table.get(text[i + j], -1)
            suffix_offset = suffix_table[j + 1]
            i += max(bad_char_offset, suffix_offset)
    return count

occurrences = boyer_moore("strrstrrstra", "str")
print(occurrences)

def rabin_karp(text, pattern):
    count = 0
    pattern_hash = sum([ord(c) for c in pattern])
    for i in range(len(text) - len(pattern)):
        substr_hash = sum([ord(c) for c in text[i:len(pattern) + i]])
        if pattern_hash == substr_hash:
            isEqual = True
            for j in range(len(pattern)):
                if text[i + j] != pattern[j]:
                    isEqual = False
                    break
            if isEqual:
                count += 1
    return count 

print(rabin_karp("strrstrrstra", "str"))

def compute_prefix_function(s):
    m = len(s)
    pi = [0] * m
    k = 0
    for i in range(len(s)):
        for j in range(1, i // 2):
            if s[:j] == s[i-j:i] and len(s[:j]) > pi[i]:
                pi[i] = len(s[:j])
    return pi

def kmp_search(text, pattern):
    count = 0
    for i in compute_prefix_function(pattern + '@' + text):
        if i == len(pattern) - 1:
            count += 1
    return count


matches = kmp_search("strrstrrstra", "str")
print(matches)

def generate_random_string(length):
    letters = string.ascii_lowercase
    rand_string = ''.join(random.choice(letters) for i in range(length))
    return rand_string

times1 = []
times2 = []
times3 = []
times4 = []
ns = [i for i in range(1000, 5000, 100)]

for i in ns:
    print(i)
    text = generate_random_string(i)
    pattern = generate_random_string(3 + (i // 10))
    start1 = time.time()
    naive(text, pattern)
    finish1 = time.time()
    times1.append(finish1 - start1)
    start2 = time.time()
    boyer_moore(text, pattern)
    finish2 = time.time()
    times2.append(finish2 - start2)
    start3 = time.time()
    rabin_karp(text, pattern)
    finish3 = time.time()
    times3.append(finish3 - start3)
    start4 = time.time()
    kmp_search(text, pattern)
    finish4 = time.time()
    times4.append(finish4 - start4)

plt.plot(ns, times1)
plt.plot(ns, times2)
plt.plot(ns, times3)
plt.plot(ns, times4)
plt.xlabel("ns")
plt.ylabel("time")
plt.legend(['naive', 'boyer_moor', 'rabin_karp', 'kmp'])
plt.show()
