import numpy as np
import scipy as sc
from sympy import *
from scipy.optimize import Bounds, minimize, LinearConstraint

x0 = 0.5
a = 0
b = 1


def y(x):
    return tan(x) / sin(x)


print('#1')

print(sc.misc.derivative(y, x0, dx=1e-6))

print('#2')

print(diff((tan(symbols('x')) / sin(symbols('x'))), symbols('x')))

print('#3')

print(sc.integrate.quad(y, a, b))

print('#4')

print(integrate(tan(symbols('x')) / sin(symbols('x')), symbols('x')))

print('#5')


def f(x):
    return (x[0] - 4) + (x[1] - 3) ** 2 + (x[2] - 2) ** 3 + (x[3] - 1) ** 4


f2 = lambda x: x[0] + 2*x[1] + 3*x[2] + 4*x[3]
x0 = np.array([1, 1, 1, 1])
bound = Bounds([0, 0, 0, 0], [np.inf, np.inf, np.inf, np.inf])
lc = LinearConstraint([1, 2, 3, 4], [0], [np.inf])
res = minimize(f, x0, method='trust-constr', constraints=[lc], bounds=bound)
print(res.x)
