package org.example.foundingBot;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class SearchBot implements ISearchBot {

    private Lock lock = new ReentrantLock();

    private final Pattern patternClass = Pattern.compile("class\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternInterface = Pattern.compile("interface\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassExtends = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassImplements = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassImplementsAfterExtends = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)");
    private final Pattern patternClassExtendsAfterImplements = Pattern.compile("class\\s+([a-zA-Z_0-9]*)\\s+implements\\s+([a-zA-Z_0-9]*)\\s+extends\\s+([a-zA-Z_0-9]*)");

    public void search(final File folder, final Map<String, List<String>> hierarchy)  {
        try {
            for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
                if (fileEntry.isDirectory()) {
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            search(fileEntry, hierarchy);
                        }
                    });
                    thread.start();
                    thread.join();
                } else {
                    final String text = String.join(" ", Files.readAllLines(Paths.get(fileEntry.getPath()), StandardCharsets.UTF_8));

                    Stream<MatchResult> patternClassResults = patternClass.matcher(text).results();
                    Stream<MatchResult> patternInterfaceResults = patternInterface.matcher(text).results();
                    Stream<MatchResult> patternClassExtendsResults = patternClassExtends.matcher(text).results();
                    Stream<MatchResult> patternClassImplementsResults = patternClassImplements.matcher(text).results();
                    Stream<MatchResult> patternClassImplementsAfterExtendsResults = patternClassImplementsAfterExtends.matcher(text).results();
                    Stream<MatchResult> patternClassExtendsAfterImplementsResults = patternClassExtendsAfterImplements.matcher(text).results();

                    lock.lock();
                    try {
                        patternClassResults.forEach(
                                res -> {
                                    hierarchy.put(res.group(1), hierarchy.getOrDefault(res.group(1), new ArrayList<>()));
                                });
                        patternInterfaceResults.forEach(
                                res -> {
                                    hierarchy.put(res.group(1), hierarchy.getOrDefault(res.group(1), new ArrayList<>()));
                                });
                        patternClassExtendsResults.forEach(
                                res -> {
                                    List<String> list = hierarchy.getOrDefault(res.group(2), new ArrayList<>());
                                    list.add(res.group(1));
                                    hierarchy.put(res.group(2), list);
                                }
                        );
                        patternClassImplementsResults.forEach(
                                res -> {
                                    List<String> list = hierarchy.getOrDefault(res.group(2), new ArrayList<>());
                                    list.add(res.group(1));
                                    hierarchy.put(res.group(2), list);
                                }
                        );
                        patternClassImplementsAfterExtendsResults.forEach(
                                res -> {
                                    List<String> list = hierarchy.getOrDefault(res.group(3), new ArrayList<>());
                                    list.add(res.group(1));
                                    hierarchy.put(res.group(3), list);
                                }
                        );
                        patternClassExtendsAfterImplementsResults.forEach(
                                res -> {
                                    List<String> list = hierarchy.getOrDefault(res.group(3), new ArrayList<>());
                                    list.add(res.group(1));
                                    hierarchy.put(res.group(3), list);
                                }
                        );
                    } finally {
                        lock.unlock();
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Can't open " + e.getMessage());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    };
}
