# pip install gensim+
9

from gensim.models import Word2Vec
from nltk.tokenize import word_tokenize

text = ['Huba-buba give you a respect']

tokenized = [word_tokenize(x.lower()) for x in text]

w2v = Word2Vec(min_count=1, vector_size=52, window=2)
w2v.build_vocab(tokenized)
# w2v.train(tokenized)
w2v.train(tokenized, total_examples=w2v.corpus_count, epochs=1)

print(w2v.wv['respect'])
