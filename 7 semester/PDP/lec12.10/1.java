public class Main {
    public static void main(String[] args) throws InterruptedException {
        String message = "Hello, world!";

        // 1
        new Thread(() -> System.out.println(message)).start();

        //2
        Thread t = new Thread(new MyRunnable(message)).start();
        t.start();
        t.join();
        message = "abc";

        //3
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("abc");
            }
        }).start();

        // interrupted exception

        Thread t = new Thread(() -> {
            while (true) {
                try {
                    longRunngingMethod();
                } catch (InterruptedException e) {
                    break;
                }
            }
            System.out.println("I'm done")
        });
        t.start();


        Thread.sleep(3000);
        System.out.println("Please, stop");
        t.interrupt();
        t.join();
        System.out.println("Completed");
    }

    public static void longRunngingMethod1() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new InterruptedException();
        }
    }

    public static void longRunngingMethod2() {
        Thread.sleep(1000);
    }

    public static void longRunngingMethod3() {
        while (true) {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
        }
    }

    Thread t = new Thread(() -> {
            while (true) {
                // try {
                //     longRunngingMethod3();
                // } catch (InterruptedException e) {
                //     Thread.currentThread().interrupt();
                //     break;
                // }
                if (Thread.interrupted()) {
                    Thread.currentThread().interrupt();
                    break;
                }
            }
            System.out.println("I'm done")
        });
        t.start();

    public static class MyRunnable implements Runnable {
        private String message;
        public MyRunnable(String message) {
            this.message = message;
        }
        @Override
        public void run() {
            System.out.println(message);
        }
    }
}