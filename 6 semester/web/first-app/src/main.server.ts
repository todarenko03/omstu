import { bootstrapApplication } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { config } from './app/app.config.server';
import { CounterComponent } from './counter/counter.component';
import { Buttons2 } from './counter2/buttons2.component';
import { Buttons3 } from './with-service/buttons3.component';

// const bootstrap = () => bootstrapApplication(Buttons2);
const bootstrap = () => bootstrapApplication(Buttons3);

export default bootstrap;
