package org.example.transformer;

public class Transformer implements ITransformer {

    public Transformer() {}

    public String transform(final String text, final String template, final String replacing) {
        return text.replaceAll(template, replacing);
    }
}
