

class Ant:
    def __init__(self, start_point):
        self.direction = 1
        self.coords = start_point

    def move(self):
        if self.direction == 1:
            self.coords[1] += 1
        elif self.direction == 2:
            self.coords[0] -= 1
        elif self.direction == 3:
            self.coords[1] -= 1
        else:
            self.coords[0] += 1

    def rotate(self, d):
        if d == 1:
            self.direction += 1
            if self.direction > 3:
                self.direction = 0
        else:
            self.direction -= 1
            if self.direction < 0:
                self.direction = 3
