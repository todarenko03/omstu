import torch
import torch.nn as nn

class Classification(nn.Module):
    def __init__(self, in_size, hidden_size, n_layers, out_size):
        super(Classification, self).__init__()

        self.lstm = nn.LSTM(in_size, hidden_size, n_layers)
        self.ff = nn.Linear(hidden_size, out_size)
        self.softmax = nn.Softmax(dim=2)

    def forward(self, x):
        out, _ = self.lstm(x)
        res = self.ff(out)
        res = self.softmax(res)

        return res
    
in_size = 5
hidden_size = 16
n_layers = 4
out_size = 3

model = Classification(in_size=in_size, hidden_size=hidden_size, n_layers=n_layers, out_size=out_size)

data = torch.randn(1, 5, in_size)

out = model(data)
print(data)
print(out)
