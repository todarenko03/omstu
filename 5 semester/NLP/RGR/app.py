from fastapi import FastAPI
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
from transformers import AutoTokenizer, AutoModelForSequenceClassification
from pydantic import BaseModel

app = FastAPI()
tokenizer = AutoTokenizer.from_pretrained("Hello-SimpleAI/chatgpt-detector-roberta")
model = AutoModelForSequenceClassification.from_pretrained("Hello-SimpleAI/chatgpt-detector-roberta")

@app.get("/")
async def main():
    return FileResponse("templates/index.html")

@app.get("/request")
def request():
    return FileResponse("templates/request.html")

class InputData(BaseModel):
    text: str

@app.post("/predict")
def predict(data: InputData):
    model_inputs = tokenizer(data.text, return_tensors='pt')
    outputs = model(**model_inputs)
    return result_interpretation(outputs.logits.tolist())

def result_interpretation(data):
    if data[0][0] < data[0][1]:
        return "Текст написан чатом GPT"
    else:
        return "Текст написан человеком"

app.mount("/templates", StaticFiles(directory="templates"), name="templates")
