import time
import random
import matplotlib.pyplot as plt

def nayran(l: list[int]) -> list[int]:
    is_done = False
    while not is_done:
        is_done = True
        
        for i in range(len(l) - 1, -1, -1):
            is_break = False
            
            for j in range(i - 1, -1, -1):
                if l[j] < l[i]:
                    is_break = True
                    l[i], l[j] = l[j], l[i]
                    l[j+1:] = l[j+1:][::-1]
                    break
                
            if is_break:
                is_done = False
                break
        if is_done:
            return l

# print(nayran([1, 2, 3, 4]))

def jones(els: list[int], traektories: list[int]) -> list[int]:
    is_done = False
    while not is_done:
        is_done = True
        is_finded = False 
        maximum_list = []
        maximum = 0
        for i in range(len(els) - 1, -1, -1):
            if 0 <= i + traektories[i] < len(els) and els[i + traektories[i]] < els[i] and els[i] > maximum:
                maximum = els[i]
                maximum_list = [i, i + traektories[i]]
                is_done = False
                is_finded = True
        if is_finded:
            els[maximum_list[0]], els[maximum_list[1]] = els[maximum_list[1]], els[maximum_list[0]]
            traektories[maximum_list[0]], traektories[maximum_list[1]] = traektories[maximum_list[1]], traektories[maximum_list[0]]
            for i in range(len(els)):
                if els[i] > maximum:
                    if traektories[i] == 1:
                        traektories[i] = -1
                    else:
                        traektories[i] = 1
    return els

# print(jones([1, 3, 2, 4, 5], [-1, 1, -1, 1, -1]))

def inversion(els: list[int]) -> list[int]:
    result = []
    numbers = [i + 1 for i in range(len(els))]
    for i in range(len(els) - 1, -1, -1):
        el = els.pop(i)
        result.append(numbers.pop(-el - 1))
    return result[::-1]

# print(inversion([0, 0, 2, 1, 1]))

nayran_times = []
jones_times = []
inversion_times = []

# for i in range(3, 10):
#     els = random.sample(range(i), i)
#     time11 = time.time()
#     nayran(els)
#     time12 = time.time()
#     nayran_times.append(time12 - time11)
#     els = random.sample(range(i), i)
#     time21 = time.time()
#     jones(els, [-1 for _ in range(i)])
#     time22 = time.time()
#     jones_times.append(time22 - time21)
#     time31 = time.time()
#     inversion([random.randint(0, j - (j // 2)) for j in range(i)])
#     time32 = time.time()
#     inversion_times.append(time32 - time31)
    
# plt.plot([i for i in range(3, 10)], nayran_times)
# plt.plot([i for i in range(3, 10)], jones_times)
# plt.plot([i for i in range(3, 10)], inversion_times)
# plt.xlabel('Размер списка')
# plt.ylabel('Время выполнения')
# plt.legend(['Найран', 'Джонс', 'Инверсия'])
# plt.show()

def all_swaps(l: list[int]) -> list[int]:
    l = sorted(l)
    print(l)
    is_done = False
    while not is_done:
        is_done = True
        
        for i in range(len(l) - 1, -1, -1):
            is_break = False
            
            for j in range(i - 1, -1, -1):
                if l[j] < l[i]:
                    is_break = True
                    l[i], l[j] = l[j], l[i]
                    l[j+1:] = l[j+1:][::-1]
                    print(l)
                    break
                
            if is_break:
                is_done = False
                break
        if is_done:
            return l

all_swaps([1, 1, 2, 1])

# swaps_times = []
# for i in range(1, 10):
#     time1 = time.time()
#     all_swaps([random.randint(0, i) for i in range(i)])
#     time2 = time.time()
#     swaps_times.append(time2-time1)

# plt.plot([i for i in range(1, 10)], swaps_times)
# plt.xlabel('Размер списка')
# plt.ylabel('Время выполнения')
# plt.show()

def all_variants(els: list) -> list:
    for i in range(1, len(els) + 1):
        for j in range(len(els)-i+1):
            print(els[j:j+i])

swaps_times = []
for i in range(1, 50):
    time1 = time.time()
    all_variants([random.randint(0, i) for i in range(i)])
    time2 = time.time()
    swaps_times.append(time2-time1)

plt.plot([i for i in range(1, 50)], swaps_times)
plt.xlabel('Размер списка')
plt.ylabel('Время выполнения')
plt.show()
            
# all_variants(['стол', 'стул', 'шкаф'])

def optimal_shopping(els: list[str, int, int], balance: int) -> list[str, int]:
    preps_els = []
    for el in els:
        for i in range(el[1]):
            preps_els.append([el[0], el[2]])
    res_price = 1e+10
    temp_variance = []
    for i in range(1, len(preps_els) + 1):
        for j in range(len(preps_els) - i + 1):
            temp_sub_list = preps_els[j:j+i]
            temp_sum = sum(x[1] for x in temp_sub_list)
            temp_sub_list_str = [x[0] for x in temp_sub_list]
            temp_variance_str = [x[0] for x in temp_variance]
            if (
                len(set(temp_sub_list_str)) > len(set(temp_variance_str)) or 
                (len(set(temp_sub_list_str)) == len(set(temp_variance_str)) and len(temp_sub_list_str) > len(temp_variance_str)) or 
                (len(set(temp_sub_list_str)) == len(set(temp_variance_str)) and len(temp_sub_list_str) == len(temp_variance_str) and temp_sum < res_price)
            ) and temp_sum <= balance:
                temp_variance = temp_sub_list
                res_price = temp_sum
    return temp_variance, res_price
            
    
# print(optimal_shopping([['тетрадь', 3, 10], ['пенал', 1, 15], ['ручка', 2, 5]], 54))
