public class Main {
    public static void main(String[] args) {
        Map<String, List<Long>> data = new HashMap();
        map("abc xyz, abc", (k, v) -> data.computeIfAbsent());
    }
    
    public static void map(String source, BiConsumer<String, Long> consumer) {
        Stream.of(source.split(" "))
            .forEach(word -> consumer.accept(word, 1L));
    }

    public static void reduce(String key, List<Long> values) {
        long sum = values.stream()
            .mapToLong(Long::valueOf)
            .sum();
        // System.out.println(key + ": " +sum);
        consumer.accept(key, sum);
    }
}