import numpy as np
import pandas as pd
import statistics
from collections import Counter
import matplotlib.pyplot as plt
import seaborn as sns

data = np.array([32.7, 27.6, 35.1, 45.5, 40.7, 20.7, 21.0, 33.8, 37.5, 32.1, 47.7, 11.3, 35.5, 35.3, 36.2, 40.9, 23.2, 30.4, 
                30.0, 49.6, 36.0, 34.0, 27.0, 31.7, 54.6, 48.9, 21.3, 49.2, 35.5, 43.3, 33.6, 27.5, 23.0, 32.6, 19.6, 41.5, 
                37.4, 38.6, 23.8, 19.2, 23.7, 38.5, 34.4, 27.6, 35.1, 26.0, 16.4, 28.0, 39.7, 42.1, 32.7, 39.0, 21.9, 42.2, 
                50.6, 34.6, 33.9, 49.4, 43.7, 35.9])

variance_row = sorted(data) 
print(pd.DataFrame(variance_row, columns=['Element']))
r = max(data) - min(data)
print('размах ', r)
print('x0 ', min(data))
print('max', max(data))

interval_variance_row = [[['7,8 - 14,8'], []], [['14,8 - 21,8'], []], 
                         [['21,8 - 28,8'], []], [['28,8 - 35,8'], []],
                         [['35,8 - 42,8'], []], [['42,8 - 49,8'], []],
                         [['49,8 - 56,8'], []]]

for i in variance_row:
    if i >= 49.8:
        interval_variance_row[6][1].append(i) 
    elif i >= 42.8:
        interval_variance_row[5][1].append(i)
    elif i >= 35.8:
        interval_variance_row[4][1].append(i)
    elif i >= 28.8:
        interval_variance_row[3][1].append(i)
    elif i >= 21.8:
        interval_variance_row[2][1].append(i)
    elif i >= 14.8:
        interval_variance_row[1][1].append(i)
    elif i >= 7.8:
        interval_variance_row[0][1].append(i)

print(pd.DataFrame(interval_variance_row, columns=['interval', 'values']))

statistical_row = Counter(variance_row)
stat_values = [(x / 60) for x in statistical_row.values()]
stat_values = np.array(stat_values)
stat_values = stat_values.reshape(56, -1)
plt.title("Полигон относительных частот")
plt.xlabel("Значение")
plt.ylabel("Частота")
plt.grid()
plt.plot(statistical_row.keys(), stat_values)
plt.scatter(statistical_row.keys(), stat_values, label='Точки')
plt.show()

plt.title('Гистограмма относительных частот')
plt.xlabel('Значения')
plt.ylabel('Относительные частоты')
plt.hist(data, bins=7, density=True)
plt.show()

interval_variance_row_values = [len(x[1]) for x in interval_variance_row]
print(interval_variance_row_values)
empirical_func = [0 for i in range(7)]
empirical_func[0] = interval_variance_row_values [0]
print(len(list(interval_variance_row_values )))
for i in range(1, len(interval_variance_row_values )):
    empirical_func[i] = empirical_func[i - 1] + interval_variance_row_values [i]

hist, edges = np.histogram(empirical_func, bins=len(empirical_func))
Y = hist.cumsum()
for i in range(len(Y)):
    plt.plot([edges[i], edges[i+1]], [Y[i], Y[i]], c='blue')
plt.show()

sample_mean = sum(variance_row) / len(variance_row)
print('Выборочное среднее', sample_mean)

variance = statistics.variance(variance_row)
corrected_variance = ((len(variance_row) - 1) / len(variance_row)) * variance
print('Исправленная выборочная дисперсия', corrected_variance)

mode = statistical_row.most_common(1)[0][0]
print("Мода ", mode)

median = variance_row[(len(variance_row) // 2) + 1]
print('Медиана', median)

std = np.sqrt(variance)
assymetry = (sum([(x - sample_mean) ** 3 for x in variance_row]) / len(variance_row)) / std ** 3
print('Асимметрия ', assymetry)

excess = ((sum([(x - sample_mean) ** 4 for x in variance_row]) / len(variance_row)) / std ** 4) - 3
print('Эксцесс ', excess)

sns.set_style('whitegrid')
sns.kdeplot(variance_row, fill=True)
plt.xlabel("Значения")  
plt.ylabel("Плотность")  
plt.title("График плотности распределения")  
plt.show() 



# 3 сигма, эксцесс и ассимметрия близки к нулю
# метод макс правдоподобия
print(sample_mean - 3 * std)
print(sample_mean + 3 * std)