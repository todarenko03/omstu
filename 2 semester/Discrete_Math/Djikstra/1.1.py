class Graph(object):
    def __init__(self, nodes):
        self.graph = [[-1 for i in range(nodes)] for j in range(nodes)]

    def add_node(self, start, finish, count):
        self.graph[start-1][finish-1] = count

    def dijkstra(self, start, finish):
        start -= 1
        finish -= 1
        road = []
        ended = set()
        sum = 0
        Ways = []
        currentArr = []
        for i in range(len(self.graph)):
            currentArr.append(float('inf'))
        currentArr[start] = 0
        Ways.append(currentArr)
        z = 0
        while len(ended) != len(self.graph):
            nextArr = [float('inf') for i in range(len(self.graph))]
            Ways.append(nextArr)
            min = float('inf')
            for i in range(len(Ways[z])):
                if (Ways[z][i] <= min) and (i not in ended):
                    min = Ways[z][i]
                    imin = i
            for i in range(len(self.graph)):
                if self.graph[imin][i] != -1:
                    if Ways[z][imin] + self.graph[imin][i] < Ways[z][i]:
                        Ways[z+1][i] = Ways[z][imin] + self.graph[imin][i]
            ended.add(imin)
            for i in range(len(self.graph)):
                if i in ended:
                    Ways[z+1][i] = Ways[z][i]
                if Ways[z][i] < Ways[z+1][i]:
                    Ways[z + 1][i] = Ways[z][i]
            z += 1
        for i in range(len(Ways[z])):
            print(Ways[z][i])
        road.append(finish)
        a = finish
        while start not in road:
            Ways[z][a]
            for i in range(len(Ways)):
                if Ways[z-1][i] + self.graph[i][a] == Ways[z][a]:
                    road.append(i)
                    a = i
                z -= 1
                if z == 0:
                    break
        for i in range(len(road)):
            road[i] += 1
        road.reverse()
        print(road)


graph = Graph(5)
graph.add_node(1, 2, 10)
graph.add_node(1, 3, 30)
graph.add_node(1, 4, 50)
graph.add_node(1, 5, 10)
graph.add_node(3, 5, 10)
graph.add_node(4, 2, 40)
graph.add_node(4, 3, 20)
graph.add_node(5, 1, 10)
graph.add_node(5, 3, 10)
graph.add_node(5, 4, 30)
graph.dijkstra(1, 5)
