#! pip install bigartm
#! pip install bertopic -q
import re
import pandas as pd
from nltk import word_tokenize
from nltk.corpus import stopwords
import string
import nltk
from nltk.stem import WordNetLemmatizer
from bertopic import BERTopic
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import fetch_20newsgroups

dataset = fetch_20newsgroups(remove=('headers', 'footers', 'qutes'))

df = pd.DataFrame({'text': dataset.data, 'target': dataset.target})

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

def preprocess(text: str) -> str:
    pattern = re.compile(r'[^a-z]+')
    text = text.lower()
    text = pattern.sub(' ', text).strip()

    word_list = word_tokenize(text)
    stopwords_list = set(stopwords.words('english'))

    word_list = [word for word in word_list if word not in stopwords_list]
    word_list = [word for word in word_list if len(word) > 2]

    lemma = WordNetLemmatizer()
    word_list = [lemma.lemmatize(word) for word in word_list]
    text= ' '.join(word_list)

    return text

df['text'] = df['text'].apply(lambda x: preprocess(x))
docs = df['text']

topic_model = BERTopic(embedding_model='distiluse-base-multilingual-cased-v1', n_gram_range=(1,3), top_n_words=6, calculate_probabilities=True)
topics, probs = topic_model.fit_transform(docs)
print(topic_model.get_topic_info())
