import time
import random
import numpy as np
import matplotlib.pyplot as plt

def build_graph(machines, n_works):
    graph = np.zeros((n_works, n_works))
    for machine in machines:
        for i in range(len(machine)):
            for j in range(i + 1, len(machine)):
                a, b = machine[i] - 1, machine[j] - 1
                graph[a, b] = 1
                graph[b, a] = 1
    return graph

def work_distribution(machines, n_works):
    graph = build_graph(machines, n_works)
    colors = [-1] * n_works
    for i in range(n_works):
        for j in range(n_works):
            if graph[i, j] == 1 and colors[j] >= colors[i]:
                colors[i] = colors[j] + 1


machines = [[1, 4, 6, 10],
            [2, 3, 7, 9],
            [2, 5, 8],
            [6, 9],
            [1, 5, 9],
            [4, 8, 10],
            [3, 4, 6],
            [1, 8],
            [3, 4, 6, 7, 10]]
n_works = 10
print(work_distribution(machines, n_works))

def generate_item(n):
    nodes = np.arange(1, n+1)
    item = []
    for _ in range(np.random.randint(1, high = n-2)):
        node = np.random.choice(nodes)
        item.append(node)
        nodes = np.delete(nodes, np.where(nodes == node))
    return item


def generate_items(n_items,n):
    return [generate_item(n) for _ in range(n_items)]


times1 = []
x = [i for i in range(10, 1000, 100)]
for i in x:
    print(i)
    start1 = time.time()
    work_distribution(generate_items(i // 3, i), i)
    finish1 = time.time()
    times1.append(finish1 - start1)

plt.plot(x, times1)
plt.xlabel = "size"
plt.ylabel = "time"
plt.show()
