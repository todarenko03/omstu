#!/usr/bin/env python3

import streamlit as st

import pages


def main() -> None:
    pages_list = ["Описание задачи", "Запрос на построение"]

    page = st.sidebar.selectbox("Выберите страницу", pages_list)

    if page == pages_list[0]:
        pages.about(pages_list[0])
    elif page == pages_list[1]:
        pages.request(pages_list[1])


if __name__ == "__main__":
    main()
