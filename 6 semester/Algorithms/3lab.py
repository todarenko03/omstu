import time
import matplotlib.pyplot as plt


def is_valid_move(x, y, visited):
    return 0 <= x < n and 0 <= y < n and visited[x][y] == 0

def knight_moves(n):
    def knight_move(x, y, move_count):
        nonlocal solution_founded, board
        if move_count == n * n + 1:
            solution_founded = True
            return True
        for dx, dy in moves:
            dx += x
            dy += y
            if is_valid_move(dx, dy, board):
                board[dx][dy] = move_count
                if knight_move(dx, dy, move_count + 1):
                    return True
                board[dx][dy] = 0
    board = [[0 for _ in range(n)] for _ in range(n)]
    moves = [(2, 1), (1, 2), (-1, 2), (-2, 1),
             (-2, -1), (-1, -2), (1, -2), (2, -1)]
    move_count = 2
    solution_founded = False
    board[0][0] = 1
    knight_move(0, 0, move_count)

    if solution_founded:
        return board
    else:
        return "Решение не найдено"


def print_board(board):
    for row in board:
        print(row)
    print()


times = []

for n in range(5, 9, 1):
    start = time.time()
    res = knight_moves(n)
    finish = time.time()
    times.append(finish - start)
    print_board(res)

n = [i for i in range(5, 9, 1)]
plt.plot(n, times)
plt.xlabel('Размер доски')
plt.ylabel('Время выполнения')
plt.show()
