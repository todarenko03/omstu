train_path = '../data/transformer/TinyStories-train.txt'
test_path = '../data/transformer/TinyStories-valid.txt'

with open(train_path, 'r', encoding='utf-8') as file:
    train_txt = file.read()
with open(test_path, 'r', encoding='utf-8') as file:
    test_txt = file.read()

train_txt = train_txt.replace('\n', '')
test_txt = test_txt.replace('\n', '')

train_txt = train_txt.replace('<|endoftext|>', '\n')
test_txt = test_txt.replace('<|endoftext|>', '\n')

with open("../data/transformer/TinyStories-train-prepr.txt", 'w', encoding='utf-8') as file:
    file.write(train_txt)
with open("../data/transformer/TinyStories-valid-prepr.txt", 'w', encoding='utf-8') as file:
    file.write(test_txt)
