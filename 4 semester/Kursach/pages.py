import streamlit as st
import numpy as np
import pandas as pd

import modeling

def read_matrix_from_table(df):
    matrix = df.values
    return np.array(matrix)


def about(title: str) -> None:
    st.title(title)

    st.header("Описание задачи")
    task = \
    """
        Исходная задача состоит в том, чтобы расположить на текстолите микросхемы заданной площади.
        Нужно распложить блоки так, чтобы как можно больше блоков, между которыми установлена связь,
        находились рядом и кроме того компактно. Также сервис предоставляет визуализацию расположения.
    """
    st.markdown(task)

    st.header("Описание данных")
    data = \
    """
        Предоставляемые данные:
        * Количество микросхем, размещаемых на плате
        * Площадь каждой микросхемы
        * Размер платы
        * Связи между блоками

        Выходные данные:
        * Лучший геном
        * Значение приспособленности
        * Визуализация расположения
    """
    st.markdown(data)


def request(title: str) -> None:
    st.title(title)

    st.header("Запрос")

    num_blocks = st.number_input("Количество микросхем", step=1)

    chip_size = st.number_input(f"Размер текстолита: ", step=1)
    block_sizes = []

    for i in range(int(num_blocks)):
        st.write(f"Схема {i+1}")
        width = st.number_input(f"Длина схемы {i+1}:", step=1)
        height = st.number_input(f"Ширина схемы {i+1}:", step=1)
        block_sizes.append((width, height))

    df = pd.DataFrame(np.zeros((num_blocks, num_blocks)))
    st.write('Матрица связей между блоками')
    connections_table = st.table(df)
    connections_table.dataframe = df
    for i in range(num_blocks):
        for j in range(i, num_blocks):
            if i != j:
                value = st.number_input(f"Наличие связи между блоками [{i + 1} и {j + 1}]", step=1)
                df.iloc[i, j] = value
                df.iloc[j, i] = value

    if st.button("Моделировать"):
        connections = read_matrix_from_table(connections_table.dataframe)
        best_genome = modeling.floorplanning_genetic_algorithm(num_blocks, chip_size, block_sizes, connections)
        best_fitness = modeling.calculate_fitness(best_genome, chip_size, block_sizes, connections)
        fig, best_genome, best_fitness = modeling.visualize_floorplan(best_genome, best_fitness, chip_size, block_sizes)
        
        if fig == None or best_fitness == None or best_genome == None:
            st.write('Блоки не могут быть размещены на микросхеме')
        
        else:
            st.write('Лучший геном: ', best_genome)
            st.write('Значение приспособленности', best_fitness)
            st.plotly_chart(fig)
