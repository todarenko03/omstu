import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RecordControlsComponent } from '../button/RecordControlsComponent';
import { DataTableComponent } from '../table/DataTableComponent';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RecordControlsComponent, DataTableComponent, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  records: any[] = [
    { column1: 'Значение 1', column2: 'Значение 2', column3: 'Значение 3', column4: 'Значение 4' }
  ];

  onAddRecord() {
    this.records.push({ column1: 'Новое значение', column2: 'Новое значение', column3: 'Новое значение', column4: 'Новое значение' });
  }

  onDeleteRecord() {
    if (this.records.length > 0) {
      this.records.pop();
    }
  }
}
