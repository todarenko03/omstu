class Elevator:
    def __init__(self, num_of_floors=3, init_position=1):
        self.num_of_floors = num_of_floors
        self.init_position = init_position

    def move_up():
        pass

    def move_down():
        pass

    def open_door():
        pass

    def close_door():
        pass