import { Component, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'record-controls',
    standalone: true,
    template: `
        <div>
            <button (click)="addRecord.emit()">+</button>
            <button (click)="deleteRecord.emit()">-</button>
        </div>
    `
})
export class RecordControlsComponent {
    @Output() addRecord = new EventEmitter<void>();
    @Output() deleteRecord = new EventEmitter<void>();
}
