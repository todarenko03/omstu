import re

from .Token import Token

class Lexer:
    def __init__(self):
        self.tokens = []
    
    def tokenize(self, text: str):
        token_specification = [
            ('NUMBER',   r'\d+'),          
            ('ID',       r'[A-Za-z]+'),    
            ('BOOLEAN',  r'\b(true|false)\b'),
            ('PLUS',     r'\+'),           
            ('MINUS',    r'-'),            
            ('MULT',     r'\*'),           
            ('DIV',      r'/'),            
            ('LBRACKET',   r'\('),           
            ('RBRACKET',   r'\)'),           
            ('SKIP',     r'[ \t.]+'),                  
        ]
        tok_regex = '|'.join(f'(?P<{pair[0]}>{pair[1]})' for pair in token_specification)
        for mo in re.finditer(tok_regex, text):
            kind = mo.lastgroup
            value = mo.group()
            if kind == 'SKIP':
                continue
            else:
                self.tokens.append(Token(kind, value))
        self.tokens.append(Token('EOF', None))
        return self.tokens
