import numpy as np
import plotly
import plotly.express as px
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import pandas as pd
from scipy import linalg, stats

#1


def y(x):
    return np.tan(x) / np.sin(x)


a, b = 0, 1
step = (b - a) / 10

answer = []
points = []

a += step

while a < b:
    points.append(a)
    answer.append(y(a))
    a += step

print(answer)

#2

fig1 = go.Figure()
fig1.add_trace(go.Scatter(x=points, y=answer, name='$$y(x) = tan(x) / sin(x)$$'))
fig1.update_layout(showlegend=True,
                  xaxis_title="X",
                  yaxis_title="Y")
fig1.show()

#3

fig2 = go.Figure()
fig2.add_trace(go.Scatter(x=points, y=answer, mode='markers',
                          name='y(x)=tan(x)/sin(x)',
                          marker_color='rgb(200, 30, 100)'))
fig2.update_layout(showlegend=True,
                  xaxis_title="X",
                  yaxis_title="Y")
fig2.show()

#4

sample1 = stats.norm.rvs(size=100)
sample2 = stats.uniform.rvs(size=100)

fig3 = go.Figure(go.Histogram(x=sample1, marker_color='rgb(200, 30, 100)'))
fig3.update_layout(showlegend=True)
fig4 = go.Figure(go.Histogram(x=sample2, marker_color='rgb(50, 210, 140)'))
fig4.update_layout(showlegend=True)
fig3.show()
fig4.show()

#5

sample3 = np.random.randint(1, 5, size=50)


fig5 = go.Figure()
fig5.add_trace(go.Pie(values=sample3))
fig5.update_layout(showlegend=True,
                   title="Круговая диаграмма")
fig5.show()
unique, counts = np.unique(sample3, return_counts=True)
fig6 = go.Figure(data=[go.Bar(x=unique, y=counts)])
fig6.show()

#6

def f(x, y):
    return x + 2*y
x = np.random.random(10)
y = np.random.random(10)
fig7 = go.Figure(data=[go.Mesh3d(x=x, y=y, z=f(x,y),
                 color='rgba(244,22,100)')])
fig7.update_layout(showlegend=True)
fig7.show()

#7
answer1 = np.array([])
points1 = np.array([])

a += step

while a < b:
    np.append(points1, a)
    np.append(answer1, y(a))
    a += step

fig8 = make_subplots(rows=2, cols=2, start_cell="bottom-left",
                     specs=[[{"type": "pie"}, {"type": "mesh3d"}],
                            [{"type": "xy"}, {"type": "xy"}]])
fig8.add_trace(go.Scatter(x=points, y=answer, name='$$y(x) = tan(x) / sin(x)$$'),
               row=2, col=1)
fig8.add_trace(go.Scatter(x=points, y=answer, mode='markers',
                          name='y(x)=tan(x)/sin(x)',
                          marker_color='rgb(200, 30, 100)'),
               row=2, col=2)
fig8.add_trace(go.Pie(values=sample3),
               row=1, col=1)
fig8.add_trace(go.Mesh3d(x=x, y=y, z=f(x,y)),
               row=1, col=2)
fig8.update_layout(height=900, showlegend=False,
                   title_text="Сетка")
fig8.show()

#8

fig7.update_layout(template="plotly_dark")
fig7.show()
fig7.update_layout(template="ggplot2")
fig7.show()
fig7.update_layout(template="simple_white")
fig7.show()
