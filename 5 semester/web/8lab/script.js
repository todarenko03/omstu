const data = {
    name: "Организация",
    children: [
      {
        name: "Иванов Иван Иванович",
        children: [
          {
            name: "Петров Петр Петрович",
            children: [
              { name: "Сидоров Сидор Сидорович" },
              { name: "Кузнецова Анна Ивановна" },
            ],
          },
          { name: "Смирнова Ольга Петровна" },
        ],
      },
      {
        name: "Сидорова Мария Сергеевна",
        children: [
          { name: "Андреев Андрей Андреевич" },
          { name: "Семенов Семен Семенович" },
        ],
      },
    ],
  };
  
  const $searchInput = document.getElementsByClassName("searchInput")[0];
  const $employeeList = document.getElementsByClassName("employeeList")[0];
  const $orgChart = document.getElementsByClassName("orgChart")[0];
  
  function renderTree(node, target) {
    const ul = document.createElement("ul");
    ul.className = "employeeList";
    node.children.forEach((child) => {
      const li = document.createElement("li");
      li.className = "employee";
      li.textContent = child.name;
      li.addEventListener("click", () => {
        $orgChart.innerHTML = ""; 
        renderTree(child, $orgChart);
        highlightNode(li);
      });
      ul.appendChild(li);
    });
    target.appendChild(ul);
  }
  
  function highlightNode(node) {
    const siblings = node.parentElement.children;
    for (const sibling of siblings) {
      sibling.classList.remove("selected");
    }
    node.classList.add("selected");
  }
  
  function searchEmployee(query) {
    const result = [];
    function search(node, query) {
      if (node.name.toLowerCase().includes(query.toLowerCase())) {
        result.push(node);
      }
      if (node.children) {
        node.children.forEach((child) => search(child, query));
      }
    }
    search(data, query);
    return result;
  }
  
  $searchInput.addEventListener("input", function () {
    const query = this.value;
    const searchResult = searchEmployee(query);
    $employeeList.innerHTML = "";
  
    searchResult.forEach((employee) => {
      const li = document.createElement("li");
      li.className = "employee";
      li.textContent = employee.name;
      li.addEventListener("click", () => {
        $orgChart.innerHTML = "";
        renderTree(employee, $orgChart);
        highlightNode(li);
        li.style.color = "red"; 
      });
      $employeeList.appendChild(li);
    });
  });
  
  renderTree(data, $orgChart);
  