import numpy as np
from sklearn.metrics import mean_squared_error
from scipy.special import expit 


class Dence:
    def __init__(self, neurons, activation):

        self.neurons = neurons
        self.activation = activation
        self.derivatives = {'linear': self.linear_derivative,
                            'relu': self.relu_derivative,
                            'sigmoid': self.sigmoid_derivative,
                            'tanh': self.tanh_derivative}

    def linear(self, inputs):

        return inputs

    def linear_derivative(self, inputs):

        return 1

    def relu(self, inputs):

        return np.maximum(0, inputs)

    def relu_derivative(self, inputs):
        return (inputs >= 0).astype(float)

    def sigmoid(self, inputs):
        
        return expit(inputs)

    def sigmoid_derivative(self, inputs):

        return self.sigmoid(inputs) * (1 - self.sigmoid(inputs))

    def tanh(self, inputs):

        return (2 / (1 + np.exp(-2 * inputs))) - 1

    def tanh_derivative(self, inputs):

        return 1 - self.tanh(inputs) ** 2

    def forward(self, inputs, weights, bias, activation):

        Z_curr = np.dot(inputs, weights.T) + bias

        if activation == 'relu':
            A_curr = self.relu(Z_curr)

        elif activation == 'sigmoid':
            A_curr = self.sigmoid(Z_curr)

        elif activation == 'tanh':
            A_curr = self.tanh(Z_curr)
            
        elif activation == 'linear':
            A_curr = self.linear(Z_curr)
        return A_curr, Z_curr


class Network:
    def __init__(self, e=0.01):

        self.network = np.array([])
        self.architecture = np.array([])
        self.weights = np.array([])
        self.memory = np.array([])
        self.gradient = []
        self.e = e

    def add(self, layer):

        self.network = np.append(self.network, layer)

    def _compile(self, data):

        for idx, layer in enumerate(self.network):

            if idx == 0:
                self.architecture = np.append(self.architecture, {'input_dim': data.shape[1],
                                                                  'output_dim': self.network[idx].neurons,
                                                                  'activation': self.network[idx].activation})
            else:
                self.architecture = np.append(self.architecture, {'input_dim': self.network[idx - 1].neurons,
                                                                  'output_dim': self.network[idx].neurons,
                                                                  'activation': self.network[idx].activation})

    def init_weights(self, data):

        self._compile(data)
        np.random.seed(42)

        for i in range(len(self.architecture)):
            self.weights = np.append(self.weights, {'W': np.random.uniform(-1, 1, size=
                (self.architecture[i]['output_dim'], self.architecture[i]['input_dim'])),
                                                    'b': np.zeros((1, self.architecture[i]['output_dim']))})
    def forwardprop(self, data):

        A_curr = data

        for i in range(len(self.weights)):
            A_prev = A_curr
            A_curr, Z_curr = self.network[i].forward(A_prev,
                                                     self.weights[i]['W'],
                                                     self.weights[i]['b'],
                                                     self.architecture[i]['activation'])

            self.memory = np.append(self.memory, {'inputs': A_prev, 'Z': Z_curr})

        return A_curr

    def backprop(self, predicted, actual):
        
        self.gradient = np.array([])
        errors = predicted - actual
        
        for i in reversed(range(len(self.network))):
            grad1 = np.multiply(self.memory[i]['inputs'].T, errors)
            grad2 = errors
            self.gradient = np.append(self.gradient, {'W': grad1,'b': grad2})
            errors = np.dot(errors, self.weights[i]['W']) * self.network[i].derivatives[self.architecture[i]['activation']](self.memory[i]['inputs'])
            
        self.gradient = np.flip(self.gradient)
        
    def update_weights(self, data):
        
        for i in range(len(self.weights)):
            self.weights[i]['W'] -= (self.e * self.gradient[i]['W']).T
            self.weights[i]['b'] -=  self.e * self.gradient[i]['b']
    
    def adams_update_weights(self, data, b1=0.9, b2=0.99, eps=1e-4):
        ers = pow(10,-8)
        for i in range(len(self.weights)):
            v1 = self.weights[i]['W'] * b1 + (1 - b1) * self.gradient[i]['W'].T
            v2 = self.weights[i]['b'] * b1 + (1 - b1) * self.gradient[i]['b']
            s1 = self.weights[i]['W'] * b2 + (1 - b2) * np.power(self.gradient[i]['W'].T, 2)
            s2 = self.weights[i]['b'] * b2 + (1 - b2) * np.power(self.gradient[i]['b'], 2)
            
            v1 = v1 / (1 - b1)
            v2 = v2 / (1 - b1)
            s1 = s1 / (1 - b2)
            s2 = s2 / (1 - b2)
            
            self.weights[i]['W'] -= self.e * v1 / (np.sqrt(np.abs(s1)) + eps)
            self.weights[i]['b'] -= self.e * v2 / (np.sqrt(np.abs(s2)) + eps)
            
    def train(self, X_train, y_train, epochs, adams=False):
        
        X_train = X_train.to_numpy()
        y_train = y_train.to_numpy()
        self.init_weights(X_train)
        
        for i in range(epochs):
            y_pred_res = np.array([])
            for i in range(len(X_train)):
                y_pred = self.forwardprop(X_train[i])
                y_pred_res = np.append(y_pred_res, y_pred)
                self.backprop(y_pred, y_train[i])
                if adams:
                    self.adams_update_weights(X_train[i])
                else:
                    self.update_weights(X_train[i])
            print(mean_squared_error(y_pred_res, y_train))
    def predict(self, X_test):
        return self.forwardprop(X_test)