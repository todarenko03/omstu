import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { CounterComponent } from './counter/counter.component';
import { Buttons2 } from './counter2/buttons2.component';
import { Buttons3 } from './with-service/buttons3.component';

// bootstrapApplication(Buttons2)
bootstrapApplication(Buttons3)
  .catch((err) => console.error(err));
