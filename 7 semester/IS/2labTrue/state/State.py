class State:
    def __init__(self, name):
        self.name = name

    def on_event(self, fsm, token_type):
        transition = fsm.get_transition(self, token_type)
        fsm.set_state(transition)
