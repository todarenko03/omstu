import re

def text_cleaning(text):
    expr1 = r'\b(?!100000)\d{4,5}\b'
    text = re.sub(expr1, '', text)

    expr2 = r'\b[A-Za-z]+@[A-Za-z]+\.[A-Za-z]+\b|\b[A-ZА-Я][a-zа-я]+\b'
    text = re.sub(expr2, '', text)

    return text

t = 'Яхал намахал что за тигр вообще 1000 6666 100000, вот почта его tadzhmahal@mail.ru'
print(text_cleaning(t))
