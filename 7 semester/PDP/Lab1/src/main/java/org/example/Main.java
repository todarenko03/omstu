package org.example;

import org.example.transfromer.Transformer;
import org.example.wordCounter.WordCounter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Лабораторная работа 1
 * Счетчик слов
 * В произвольном текстовом документе посчитать и напечатать в консоли сколько раз
 * встречается каждое слово. Текст можно сформировать генератором lorem ipsum.
 * Необходимо использовать регулярные выражения (regexp), должны корректно
 * обрабатываться любые знаки препинания в любом количестве.
 */

public class Main {
    public static void main(String[] args) {
        try {
            final String text = String.join(" ", Files.readAllLines(Paths.get("text.txt"), StandardCharsets.UTF_8));
            final String template = "[^A-Za-zА-Яа-я0-9']";
            final String spaceTemplate = " +";
            final String replacing = " ";

            Transformer transformer = new Transformer();
            String preFormattedText = transformer.transform(text, template, replacing);
            String formattedText = transformer.transform(preFormattedText, spaceTemplate, replacing);

            WordCounter wordCounter = new WordCounter();

            Map<String, Integer> wordsMap = wordCounter.calculate(formattedText);

            wordsMap.forEach((key, value) -> System.out.println(key + ": " + value));
        } catch (IOException e) {
            System.out.println("Can't read " + e.getMessage());
        }
    }
}
